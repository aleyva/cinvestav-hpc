
#include <stdio.h>
#include <time.h>
#include <chrono>
#include <omp.h>
#include <stdlib.h>
#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

struct s_ran3{
    long idum;
    int inext,inextp;
    long ma[56];
};

float ran3(struct s_ran3 *rand){
    int iff=0;
    long mj,mk;
    int i,ii,k;
    if (rand->idum < 0 || iff == 0) {
        iff = 1;
        mj = labs(MSEED-labs( rand->idum ));
        mj %= MBIG;
        rand->ma[55] = mj;
        mk = 1;
        for (i=1; i <= 54; i++) {
            ii=(21*i) % 55;
            rand->ma[ii]=mk;
            mk=mj-mk;
            if (mk < MZ)
                mk += MBIG;
            mj=rand->ma[ii];
        }
        for (k=1; k <= 4; k++)
            for (i=1; i <= 55; i++) {
                rand->ma[i] -= rand->ma[1+(i+30) % 55];
                if (rand->ma[i] < MZ) rand->ma[i] += MBIG;
            }
        rand->inext = 0;
        rand->inextp = 31;
        rand->idum = 1;
    }
    if ( ++(rand->inext) == 56) rand->inext = 1;
    if ( ++(rand->inextp) == 56) rand->inextp = 1;
    mj = rand->ma[rand->inext] - rand->ma[rand->inextp];
    if (mj < MZ) mj += MBIG;
    rand->ma[rand->inext] = mj;
    rand->idum = mj;
    return mj * FAC;
}

int main(int argc, char *argv[]){
    if(argc!=3){
        printf("<events>, <threads>\n");
        exit(-1);
    }

    // inicializar var. reloj
	std::chrono::time_point<std::chrono::system_clock> start_clock, before_pragma_clock, after_pragma_clock;		
	// tomar tiempo inicial
	start_clock = std::chrono::system_clock::now();

    int num_threads = atoi(argv[2]);
    long events = atol(argv[1]);
    long events_for = events/num_threads;

    omp_set_num_threads(num_threads);
    struct s_ran3 *rands, *temp_rands;
    rands = (struct s_ran3 *)calloc(num_threads*2, sizeof(struct s_ran3));
    temp_rands = rands;
    for(int i = 0; i < num_threads*2; i++, temp_rands++){
        temp_rands->idum = clock();
        // temp_rands->idum = 1234;     
    }

    printf("OMP section \n");
    long in=0;
    double pi = 0;
    double PI = 0;
    
    // tomar tiempo antes del pragma omp
	before_pragma_clock = std::chrono::system_clock::now();
    
    #pragma omp parallel for reduction(+:in)        

        for(int i = 0; i < omp_get_num_threads(); i++){
            double x, y;
            for(long j=0; j < events_for; j++){
                x = ran3(rands+i);
                y = ran3(rands+i+1);
                // printf("\t\tx: %10f\ty: %10f \n", x,y);              
                if(x*x + y*y <= 1)
                    in++;
            }
            //printf("\tfor: %d, in:%ld\n", i+1, in);
            //printf("Pi acumulado es : %f", pi);

        }

    pi += 4*((double)in)/((double)events);
    printf("Numero de puntos dentro: %ld de %ld \n", in, events);
    printf("Pi= %f\n", pi);

    // tomar tiempo despues del pragma omp
	after_pragma_clock = std::chrono::system_clock::now();		
	// imprimir tiempo	
	std::chrono::duration<double> elapsed_secuential_time = before_pragma_clock-start_clock;
	std::chrono::duration<double> elapsed_omp_time = after_pragma_clock-before_pragma_clock;
	printf("Tiempo total: %f\n", elapsed_secuential_time.count() + elapsed_omp_time.count());
	printf("  Tiempo parte secuencial: %f\n", elapsed_secuential_time.count());
	printf("  Tiempo parte OMP: %f\n", elapsed_omp_time.count());

    return 0;
}