// compilar 
// g++ -fopenmp pi_aprox_secuential.c -o pi_aprox_secuential.out

#include <stdio.h>
#include <time.h>
#include <chrono>
#include <omp.h>
#include <stdlib.h>
#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

struct s_ran3{
	long idum;
	int inext,inextp;
	long ma[56];	
};

float ran3(struct s_ran3 *rand){		
	int iff=0;
	long mj,mk;
	int i,ii,k;
	if (rand->idum < 0 || iff == 0) {
		iff = 1;
		mj = labs(MSEED-labs( rand->idum ));
		mj %= MBIG;
		rand->ma[55] = mj;
		mk = 1;		
		for (i=1; i <= 54; i++) {
			ii=(21*i) % 55;
			rand->ma[ii]=mk;
			mk=mj-mk;
			if (mk < MZ) 
				mk += MBIG;
			mj=rand->ma[ii];
		}
		for (k=1; k <= 4; k++)
			for (i=1; i <= 55; i++) {
				rand->ma[i] -= rand->ma[1+(i+30) % 55];
				if (rand->ma[i] < MZ) rand->ma[i] += MBIG;
			}
		rand->inext = 0;
		rand->inextp = 31;
		rand->idum = 1;
	}
	if ( ++(rand->inext) == 56) rand->inext = 1;
	if ( ++(rand->inextp) == 56) rand->inextp = 1;
	mj = rand->ma[rand->inext] - rand->ma[rand->inextp];
	if (mj < MZ) mj += MBIG;
	rand->ma[rand->inext] = mj;
	rand->idum = mj;
	return mj * FAC;
}

int main(int argc, char *argv[]){
	if(argc!=2){
		printf("<events>\n");
		exit(-1);
	}
	
	// inicializar var. reloj
	std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;		
	// tomar tiempo inicial
	start_clock = std::chrono::system_clock::now();

	int num_threads = 1;
	long events = atol(argv[1]);		    		
	long in=0;			 	

    struct s_ran3 *rands1, *rands2;
	rands1 = (struct s_ran3 *)calloc(num_threads*2, sizeof(struct s_ran3));
	rands2 = (struct s_ran3 *)calloc(num_threads*2, sizeof(struct s_ran3));
	
	double x, y;		
	for(long j=0; j < events; j++){
		x = ran3(rands1);	
		y = ran3(rands2);	
		// printf("\t\tx: %10f\ty: %10f \n", x,y);				
		if(x*x + y*y <= 1)
			in++;
	}   		

	printf("Numero de puntos dentro: %ld de %ld \n", in, events);
	printf("Pi= %f\n", 4*((double)in)/((double)events));

	// tomar tiempo despues del pragma omp
	end_clock = std::chrono::system_clock::now();		
	// imprimir tiempo	
	std::chrono::duration<double> elapsed_time = end_clock-start_clock;	
	printf("Tiempo total: %f\n", elapsed_time.count());	
	
	return 0;
}
