#include <stdio.h>
#include <time.h>
#include <math.h>
#define NB 4
#define NT 64

	
__global__ void kernelTransponer(double *a, double *at, int n){
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int j = threadIdx.y + blockIdx.y*blockDim.y; 
	while (i < n) {
       j = threadIdx.y + blockIdx.y*blockDim.y;
       while (j < n) {
           at[i*n+j] = a[j*n+i];
           j += blockDim.y*gridDim.y;
       }
       i+=blockDim.x*gridDim.x;
   }
}

__global__ void cholesky(double *out, double *mat, int diag, int n) {
	int pos = threadIdx.x + blockIdx.x*blockDim.x;
	int i=0,j=0;
	if(diag+1>n){
        i=n-1-pos;
        j=diag-i;
    }
    else{
		i = pos;
		j = diag-i;
    }	
    double s = 0;
        for (int k = 0; k < j; k++)
            s += out[i * n + k] * out[j * n + k];
		
        out[i * n + j] = (i == j) ? sqrt(mat[i * n + i] - s) : (1.0 / out[j * n + j] * (mat[i * n + j] - s));
	__syncthreads();
	if(i<j)
		out[i*n+j]=0;
}


void print_matrix(double *mat, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++)
            printf("%2.2f ", mat[i * n + j]);
        printf("\n");
    }
    printf("\n");
}

void transponer_device(double *a, double *at, int n){
	double *aD, *atD;
	int size = n*n*sizeof(double);

	dim3 bloques(3,3);
	dim3 hilos(2,2);

	cudaSetDevice(1);
	
	//1. Asignar memoria
	cudaMalloc(&aD, size);
	cudaMalloc(&atD, size);
	
	//2.- Copiar los datos del Host al Device
	cudaMemcpy(aD, a, size, cudaMemcpyHostToDevice);
	
	//3.- Ejecutar kernel
	kernelTransponer<<<bloques, hilos>>>(aD,atD,n);
	
	//4.- Copiar datos del device al host
	cudaMemcpy(at, atD, size, cudaMemcpyDeviceToHost);
	
	//5. Liberar Memoria 
	cudaFree(aD); cudaFree(atD);

}
void cholesky_in_device(double *out, double *mat, int n)
{
   double *matD, *outD;
   int size = (int)(n * n * sizeof(double));

   cudaSetDevice(0);

   // 1. Asignar memoria
   cudaMalloc(&matD, size);   
   cudaMalloc(&outD, size);

   // 2. Copiar datos del Host al Device
   cudaMemcpy(matD, mat, size, cudaMemcpyHostToDevice);   

   // 3. Ejecutar kernel
   //procesar cholesky por diagonales
	int threads=0;
	int i =0, j=0;
	int blocks = 0;
   for(int diag=0,pos=-n+1; diag<(n*2)-1; diag++,pos++){	
		if(diag+1>n){
			threads=n-pos;
			i=n-threads;
		}
		else{
        	threads = diag+1;
			i = threads-1;	
		}
		if(threads<1024)
        	cholesky<<<1, threads>>>(outD, matD, diag, n);
    }

   // 4. Copiar daros del device al Host
   cudaMemcpy(out, outD, size, cudaMemcpyDeviceToHost);

   // 5. Liberar Memoria
   cudaFree(matD);   
   cudaFree(outD);
}
void matMul(double *A, double *B, double *C, int n){
	double sum=0;
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			sum = 0;
			for(int k=0;k<n;k++){
				sum += A[j*n+k]*B[k*n+i];
			}
			C[i*n+j]=sum;
		}
	}
}

void create_symm_matrix(double *s_mat, int n){    
    double n_rand;
	double *smatT1 = (double *)malloc(n*n*sizeof(double));
	double *smatT2 = (double *)malloc(n*n*sizeof(double));
	memset(smatT1,0,n*n*sizeof(double));	
	memset(smatT2,0,n*n*sizeof(double));	
    for(int i = 0; i < n; i++){
		for(int j = i; j < n; j++){
			n_rand = (rand() % 100)-50;
			if(i<=j)
				smatT1[(i*n)+j] = s_mat[(j*n)+i] = n_rand;
			else
				smatT1[i*n+j]=0;
		}
	}
	transponer_device(smatT1,smatT2,n);
	matMul(smatT1,smatT2,s_mat,n);
}

int main(int argc, char *argv[])
{
	if(argc!=2){
		printf("<n>\n");
		exit(-1);
	}	

    clock_t time1, time2;
    int n = atoi(argv[1]);	
    
	srand(time(NULL));
        
	//double mat[n*n];
	double *mat;
	int size = n*n*sizeof(double);
	mat = (double*)malloc(size);
	memset(mat,0,size);	
	// generar matriz simétrica		
    create_symm_matrix(mat, n);
     
    //show_matrix(mat,N);
	//printf("Matriz inicial \n");
	//print_matrix(mat,N);	    

    double *out = (double*)malloc(size);
    double *outTrans= (double*)malloc(size);
    double *mul= (double*)malloc(size);

	memset(out,0,size);	
	memset(outTrans,0,size);	
	memset(mul,0,size);	
    if (out == NULL)
        exit(EXIT_FAILURE);
    
	time1 = clock();
	cholesky_in_device(out, mat, n);
	transponer_device(out,outTrans,n);
	time2 = clock();

    //print_matrix(out, N);   
	//printf("transpuesta:\n");
	//print_matrix(outTrans,N);
	//printf("Multiplicacion \n");
	matMul(out, outTrans,mul,n);
	//print_matrix(mul,n);
	free(mat);	
    free(outTrans);free(mul); free(out);
   
    
    printf("Filas: %ld", n);    
    printf("\nTotal: %f\n", ((double) (time2 - time1)) / CLOCKS_PER_SEC);

    return 0;
}

