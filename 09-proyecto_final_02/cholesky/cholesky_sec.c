#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>



void mat_mul(double *A,double*B,double *C,int n){
	double sum=0;
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			sum = 0;
			for(int k=0;k<n;k++){
				sum += A[j*n+k]*B[k*n+i];
			}
			C[i*n+j]=sum;
		}
	}
}

void transponer(double *L, double *LT, int n){
	for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				LT[i*n+j] = L[j*n+i];
			}
	}
}

void create_symm_matrix(double *s_mat, int n){    
    double n_rand;
	double *smatT1 = (double *)malloc(n*n*sizeof(double));
	double *smatT2 = (double *)malloc(n*n*sizeof(double));
	memset(smatT1,0,n*n*sizeof(double));	
	memset(smatT2,0,n*n*sizeof(double));	
    for(int i = 0; i < n; i++){
		for(int j = i; j < n; j++){
			n_rand = (rand() % 100)-50;
			if(i<=j)
				smatT1[(i*n)+j] = s_mat[(j*n)+i] = n_rand;
			else
				smatT1[i*n+j]=0;
		}
	}
	transponer(smatT1,smatT2,n);
	mat_mul(smatT1,smatT2,s_mat,n);
}

void cholesky(double *L, double *LT, double *A, int n)
{
	int i, j, k; double suma=0;;
	for(i = 0; i < n; i++)
	{
		for(j = 0; j <= i; j++)
		{
			suma = 0;
			if(i == j)
			{
				for(k = 0; k < j; k++)
					suma += pow(L[j*n+k], 2);
				L[i*n+j] = sqrt(A[j*n+j] - suma);
			}
			else
			{
				for(k = 0; k < j; k++)
					suma += L[i*n+k]*L[j*n+k];
				L[i*n+j] = (A[i*n+j] - suma)/L[j*n+j];
			}
		}
	}

	transponer(L,LT,n);	
}

void print_matrix(double *mat, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++)
            printf("%2.2f ", mat[i * n + j]);
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char *argv[])
{
    if(argc!=2){
		printf("<n>\n");
		exit(-1);
	}	

    clock_t t1, t2;
    int n = atol(argv[1]);	

	double *C, *D;
	srand(time(NULL));
	int size = n*n*sizeof(double);	
	double *L, *LT;
	C = (double*)malloc(size);
	L = (double*)malloc(size);
	LT = (double*)malloc(size);
	D = (double*)malloc(size);
	memset(C, 0, sizeof(size));
	memset(L, 0, sizeof(size));
	memset(LT, 0, sizeof(size));
	memset(D, 0, sizeof(size));
	
    
    
    create_symm_matrix(C,n);
	// printf("\n MATRIZ C\n");
	// print_matrix(C, N);	
    
    t1 = clock();
	cholesky(L, LT, C, n);
	t2 = clock();

	// printf("\n MATRIZ A\n");
	// print_matrix(L, N);

	// printf("\n MATRIZ B\n");
	// print_matrix(LT, N);
	
	mat_mul(L,LT,D,n);
	// printf("Comprobacion\n");
	// print_matrix(D,N);
	// printf("\n");
	
    
    free(D);
	free(C);free(L);free(LT);
    
    printf("Filas: %ld", n);    
    printf("\nTotal: %f\n", ((double) (t2 - t1)) / CLOCKS_PER_SEC);
    
    

	return 0;
}	