//compilacion
//g++ -c utility.cpp;
//g++ -fcilkplus merge_sort_cilk.cpp utility.o -o merge_sort_cilk.o

#include <stdio.h>
#include <stdlib.h>		// rand, NULL, atoi
#include <chrono>
#include <ctime>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include "utility.h"
//#include <mutex>

//variables globales
int level_glob;	
int if_print;
//pthread_mutex_t lock;

//prototipos de funciones
void *merge_sort(uint32 *mat, uint32 *mat_temp, int start, int end, int level);

int main(int argc, char *argv[]){ 	
	
	//variables
	int row_size;			
	int return_value;					//varible para controlar los codigos de error			

	//variables de reloj
	std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;		

/** Leer el tamaño de la matriz, el numero de threads con que se va a trabajar y la variable para impresión, estos numero se introduce como parametro al ejecutar el programa **/	
	if(argc == 4){		
		row_size = atoi(argv[1]);
		level_glob = atoi(argv[2]);
		if_print = atoi(argv[3]); 
	}else{
		printf("\nDebes introducir 3 parametros;");
		printf("\n\t- row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size");
		printf("\n\t- level (int): Nivel hasta el cual se utilizarán threads.");
		printf("\n\t- if_print (int): Impresíon. Valores; 0 para no imprimir, 1 imprimir procesos, 2 para imprimir matriz ordenada, 3 para imprimir matriz inicial. Cada número imprime también los datos anteriores");
		printf("\n");
		return 1;
	}

	uint32 *mat = (uint32 *)malloc(sizeof(uint32) * (row_size*row_size));
	uint32 *mat_temp = (uint32 *)malloc(sizeof(uint32) * (row_size*row_size));
	
/** inicializar e imprimir la matriz **/
	mat_init(mat, row_size*row_size);	
	if(if_print >= 3){ printf("\nMatriz creada:"); mat_print(mat, row_size);}

/** iniciar reloj **/	
	start_clock = std::chrono::system_clock::now();	
	
/** ordenar la matriz **/
	//establecer los valores a Args_process		
	merge_sort(mat, mat_temp, 0, (row_size*row_size)-1, 0);

/** detener reloj **/	
	end_clock = std::chrono::system_clock::now();
	
/** imprimir la matriz **/
	if(if_print >= 2){ printf("\nMatriz ordenada:"); mat_print(mat, row_size);}
	
/** verificacion **/
	if(if_print >= 1) printf("\n");
	mat_is_sort(mat, row_size*row_size);	
		
/** imprimir tiempo **/			
	int total_proc = ( level_glob * (level_glob+1) * (level_glob*2+1) ) /2 ;
	std::chrono::duration<double> elapsed_seconds = end_clock-start_clock;		
	printf("\tTiempo transcurrido: %f. %d sub-procesos creados hasta el nivel %d.\n", elapsed_seconds.count(), total_proc, level_glob);	
				   	
   	exit(EXIT_SUCCESS);
}

void *merge_sort(uint32 *mat, uint32 *mat_temp, int start, int end, int level){
	
	if (start < end){		

		//declaraciones e inicializaciones de variables
		int mid = (start + end) / 2;					

		if(level < level_glob){
			cilk_spawn merge_sort(mat, mat_temp, start, mid, level+1);
			cilk_spawn merge_sort(mat, mat_temp, mid+1, end, level+1);	
			if(if_print >= 1) printf("\nCreación de sub-procesos para nivel %d", level);
			cilk_sync;	
		}else{
			merge_sort(mat, mat_temp, start, mid, level+1);								
			merge_sort(mat, mat_temp, mid+1, end, level+1);	
		}

		merge(mat, mat_temp, start, mid, end);				
	}
}