//compilacion
//g++ -c utility.cpp;
//g++ -pthread merge_sort_thread.cpp utility.o -o merge_sort_thread.o

#include <stdio.h>
#include <stdlib.h>		// rand, NULL, atoi
#include <chrono>
#include <ctime>
#include <pthread.h>
#include <mutex>
#include "utility.h"

//variables globales y referentes a los threads
struct Thread_info *threads;		//arreglo de threads
pthread_attr_t attr;				//atributos de thread
pthread_mutex_t lock;
int level_glob;	
int if_print;
int max_thread;

struct Thread_info {    		
   pthread_t thread_id;        	/* ID returned by pthread_create() */
   int thread_num;       		/* Application-defined thread # */
   struct Args_merge_sort *Args_merge_sort; 
} Thread_info;

//prototipos de funciones
void pthread_create_merge_sort(int num_thread);
void pthread_join_merge_sort(int num_left_thread);
static void *merge_sort(void *args);

int main(int argc, char *argv[]){ 	
	
	//variables
	int row_size;			
	int return_value;					//varible para controlar los codigos de error	
	
	//variables de reloj
	std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;	
	
/** Leer parametros; tamaño de la matriz, numero de threads con que se va a trabajar y variable para impresion **/	
	if(argc == 4){		
		row_size = atoi(argv[1]);
		level_glob = atoi(argv[2]);
		if_print = atoi(argv[3]); 
	}else{
		printf("\nDebes introducir 3 parametros;");
		printf("\n\t- row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size");
		printf("\n\t- level (int): Nivel hasta el cual se utilizarán threads.");
		printf("\n\t- if_print (int): Impresíon. Valores; 0 para no imprimir, 1 imprimir threads, 2 para imprimir matriz ordenada, 3 para imprimir matriz inicial. Cada número imprime también los datos anteriores");
		printf("\n");
		return 1;
	}
	
	uint32 *mat = (uint32 *)malloc(sizeof(uint32) * (row_size*row_size));
	uint32 *mat_temp = (uint32 *)malloc(sizeof(uint32) * (row_size*row_size));
		
/** Inicializacion del entorno para trabajar con threads **/	

	//variable para manejar los argumentos de los threads
	struct Args_merge_sort *Args_merge_sort;	//parametros de thread	
	
	if(level_glob > 6)
		level_glob = 6;		
	max_thread = ( level_glob * (level_glob+1) * (level_glob*2+1) ) /2 ;
	
	//reserva de memoria para threads
	threads = (struct Thread_info *)calloc(max_thread + 1, sizeof(struct Thread_info));		
   	if (threads == NULL)
       handle_error("calloc threads");
       
    //inicializacion attr, los atributos de los threads, los atributos son los de default
	return_value = pthread_attr_init(&attr);
	if (return_value != 0)
	   handle_error_en(return_value, "pthread_attr_init");
	
	//reserva de memoria para Args_merge_sort, argumentos del thread	
	Args_merge_sort = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));
	if (Args_merge_sort == NULL)
       handle_error("calloc Args_merge_sort");
       
    //asignación de numero de thread (esto es diferente de id de thread)
    for (int tnum = max_thread; tnum >= 0; tnum--) {
       threads[tnum].thread_num = tnum;
   	}
/** Fin de inicializacion de entorno para trabajar con threads **/
	

/** inicializar e imprimir la matriz **/
	mat_init(mat, row_size*row_size);	
	if(if_print >= 3){ printf("\nMatriz creada:"); mat_print(mat, row_size);}

/** iniciar reloj **/	
	start_clock = std::chrono::system_clock::now();	
	
/** ordenar la matriz **/
	//establecer los valores a Args_merge_sort	
	set_Args_merge_sort(Args_merge_sort, mat, mat_temp, 0, (row_size*row_size)-1, 0);
	merge_sort(Args_merge_sort);

/** detener reloj **/	
	end_clock = std::chrono::system_clock::now();
	
/** imprimir la matriz **/
	if(if_print >= 2){ printf("\nMatriz ordenada:"); mat_print(mat, row_size);}
	
/** verificacion **/
	if(if_print >= 1) printf("\n");
	mat_is_sort(mat, row_size*row_size);	
		
/** imprimir tiempo **/	
	max_thread = ( level_glob * (level_glob+1) * (level_glob*2+1) ) /2 ;	
	std::chrono::duration<double> elapsed_seconds = end_clock-start_clock;		
	printf("\tTiempo transcurrido: %f. %d Theads creados hasta el nivel %d.\n", elapsed_seconds.count(), max_thread, level_glob);
	
/** liberar memoria **/			
	return_value = pthread_attr_destroy(&attr);
   	if (return_value != 0)
      	handle_error_en(return_value, "pthread_attr_destroy");
    
    free(threads);
   	exit(EXIT_SUCCESS);
}

void pthread_create_merge_sort(int num_thread){
	pthread_mutex_lock(&lock);	
		int return_value = pthread_create(&threads[num_thread].thread_id, &attr, &merge_sort, threads[num_thread].Args_merge_sort);	
	pthread_mutex_unlock(&lock);
		if (return_value != 0)
			handle_error_en(return_value, "pthread_create");
		else
			if(if_print >= 1) printf("\nConsumo hilo: %d, dir: %02x", num_thread, (unsigned)threads[num_thread].thread_id);		
}

void pthread_join_merge_sort(int num_thread){	
	int return_value = pthread_join(threads[num_thread].thread_id, NULL);
	if (return_value != 0)
		handle_error_en(return_value, "pthread_join");
	else			
		if(if_print >= 1) printf("\nEl hilo: %d, dir: %02x, procesó de la posición %d a la %d", num_thread, (unsigned)threads[num_thread].thread_id, threads[num_thread].Args_merge_sort->start, threads[num_thread].Args_merge_sort->end);									
}

static void *merge_sort(void *arguments){
	//inicializacion de variables
	struct Args_merge_sort *args = (struct Args_merge_sort *)arguments;		
	int start = args->start;
	int end = args->end;
	
	if (start < end){		

		//declaraciones e inicializacion de variables
		uint32 *mat = args->mat;
		uint32 *mat_temp = args->mat_temp;
		int level = args->level;
		int mid = (start + end) / 2;
		int num_left_thread = -1;
		int num_right_thread = -1;
		int return_value;
				
		//inicializacion y asignacion de parametros para los threads
		struct Args_merge_sort *args_left = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));
		struct Args_merge_sort *args_right = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));
		set_Args_merge_sort(args_left, mat, mat_temp, start, mid, level+1);		//asignacion de valores para el hilo izquierdo				
		set_Args_merge_sort(args_right, mat, mat_temp, mid+1, end, level+1);		//asignacion de valores para el hilo derecho		


		pthread_mutex_lock(&lock);
			//¿aun se puede consumir un thread para la rama izquierda y la derecha?
			if(level < level_glob){			
				num_left_thread = max_thread;
				max_thread--;				
				num_right_thread = max_thread;
				max_thread--;
				threads[num_left_thread].Args_merge_sort = args_left;	//parametros thread		
				threads[num_right_thread].Args_merge_sort = args_right;	//parametros thread		
			}			
		pthread_mutex_unlock(&lock);
		
		//si se reservo un numero de thread para num_left_thread, la rama izquierda se procesa con thread
		if(level < level_glob){	
			pthread_create_merge_sort(num_left_thread);
			pthread_create_merge_sort(num_right_thread);
			pthread_join_merge_sort(num_left_thread);
			pthread_join_merge_sort(num_right_thread);					
		}else{
			merge_sort(args_left);								
			merge_sort(args_right);	
		}
		
		//pthread_mutex_lock(&lock);
			merge(mat, mat_temp, start, mid, end);			
		//pthread_mutex_unlock(&lock);
		
	}
}
