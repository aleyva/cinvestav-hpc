# Ordenación de una matriz con merge sort, usando subprocesos (tbb, pthread, cilk) y/o threads (pthread)

## Introducción

Se desarrollaron varios programas en c++ usando el algoritmo merge sort para ordenar una matriz de NxN de números aleatorios.. Cada programa varía la forma en que se implementa merge sort, aunque el algoritmo es el mismo, algunos programas usan procesos, otros threads y uno usa el metodo secuencial.  
El objetivo es ver la eficiencia de los métodos utilizados, asi como las librerias implementadas, que son; tbb, pthread, cilk++.

### Programas

 - merge_sort_normal.cpp: Utiliza el algoritmo merge sort de forma SECUENCIAL.
 - merge_sort_thead.cpp: Utiliza threads de la libreria "pthread". Los threads son creados en cada llamada recursiva de merge sort, hasta el nivel N dado por el usuario como parámetro. En otras palabras, cada rama izquierda y derecha del algoritmo es procesada con un thread, siempre y cuando el nivel del arbol que se está procesando, sea menor al nivel dado por el usuario como parámetro, de lo contrario se procesa de forma secuencial.
 - merge_sort_proc.cpp: Utiliza 2 procesos y por cada proceso crea 2 threads, tanto procesos y threads son de la libreria "pthread". La rama izquierda y derecha del primer nivel del arbol es procesada con procesos (por eso es que son sólo 2), posteriormente, en el segundo nivel del arbol, la rama derecha e izquierda de cada proceso, se procesa con threads, a partir del nivel 3 en adelante, todo se procesa de forma secuencial.
 - merge_sort_cilk.cpp: Utiliza subprocesos de la librería "cilk". Los sub-procesos son creados en cada llamada recursiva de merge sort, hasta el nivel N dado por el usuario como parámetro. En otras palabras, cada rama izquierda y derecha del algoritmo es procesada con un sub-proceso, siempre y cuando el nivel del arbol que se está procesando, sea menor al nivel dado por el usuario como parámetro, de lo contrario se procesa de forma secuencial.
 - merge_sort_tbb.cpp: Lo mismo que merge_sort_cilk.cpp, pero en lugar de usar la librería "cilk", usa la librería "tbb".

### Requerimientos

Para realizar la ejecución, es **INDISPENSABLE** ejecutar en una distro **LINUX** y tener instalado:  
 - compilador de gcc
 - librería pthead
 - librería cilk++
 - librería tbb

### Compilación

Para compilar, se ejecuta
```
make
```
Si la compilación da error, es por que alguno de los requerimientos no se cumple.

### Ejecución

La ejecución se puede realizar de dos formas
 - Usar el script "run.sh"
 - Ejecutar cada binario por separado

#### Ejecución usando el script "run.sh"

Para ejecutar el script "run.sh", se deben introducir 2 parametros
 - row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size"
 - level (int): Nivel hasta el cual se utilizarán threads o procesos"

Ejemplo
```
./run.sh 1000 3
```
Donde "1000" es el tamaño de la fila de la matriz, por lo que la matriz es de tamaño 1,000 x 1,000 = 1,000,000 y "3" es el numero de niveles hasta donde se crearán hilos o threads. 

Salida ejecutando "./run.sh 1000 2"
```
--- merge_sort NORMAL ------------
*** EXITO ***	Tiempo transcurrido: 0.154572

--- merge_sort THREADS -----------
*** EXITO ***	Tiempo transcurrido: 0.404107. 15 Theads creados hasta el nivel 2.

--- merge_sort PROCESOS -----------
*** EXITO ***	Tiempo transcurrido: 0.068776. 2 Procesos, cada proceso 2 threads.

--- merge_sort CILK -------------
*** EXITO ***	Tiempo transcurrido: 0.074975. 15 sub-procesos creados hasta el nivel 2.

--- merge_sort TBB --------------
*** EXITO ***	Tiempo transcurrido: 0.060445. 15 sub-procesos creados hasta el nivel 2.
```

#### Ejecución usando los binarios

Para ejecutar los programas individualmente, el archvivo de interes, descritos a continuación.

## Programas

### merge_sort_normal.cpp

Este programa realiza la ordenación secuencial de una matriz de forma secuencial. El tamaño de matriz es definidio por el usuario mediante el parametro row_size, de está forma el tamaño total de la matriz es row_size*row_size.

* se compila con
```
make
```
* se ejecuta con
```
./merge_sort_normal.o row_size if_print
```
#### Parámetros
  - Parámetro row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size
  - Parámetro if_print (int): Impresíon. Valores; 0 para no imprimir, 2 para imprimir matriz ordenada, 3 para imprimir matriz inicial. Cada número imprime también los datos anteriores
* Salida	

Imprime si la matriz fue ordenada correctamente o si dio error, ademas del tiempo total que tomó ordenar la matriz, el tiempo es unicamente de lo que le tomó al programa realizar la ordenación

* Ejemplo de la salida ejecutando "./merge_sort_normal.o 1000 1"
```
*** EXITO ***	Tiempo transcurrido: 0.155922
```

### merge_sort_threads.cpp

Este programa realiza la ordenación de la matriz utilizando threads de la librería "pthread", aunque sólo crea threads hasta el nivel dado por el usuario como parámetro, es decir, la rama izquierda y derecha de cada nivel, son procesadas con un thread cada una, siempre y cuando el nivel de esas ramas, sea menor al introducido por el usuario.
El tamaño de matriz es definidio por el usuario mediante el parametro row_size, de está forma el tamaño total de la matriz es row_size*row_size.

* se compila con
```
make
```
* se ejecuta con
```
./merge_sort_threads.o row_size level if_print
```
#### Parámetros
  - Parámetro row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size
  - Parámetro level (int): Nivel hasta el cual se crearán threads.
  - Parámetro if_print (int): Impresíon. Valores; 0 para no imprimir, 1 imprimir threads, 2 para imprimir matriz ordenada, 3 para imprimir matriz inicial. Cada número imprime también los datos anteriores

* Salida	

Imprime si la matriz fue ordenada correctamente o si dio error, los threads utilizados y el tiempo total que tomó ordenar la matriz, el tiempo es unicamente de lo que le tomó al programa realizar la ordenación

* Ejemplo de la salida ejecutando "./merge_sort_thread.o 1000 2 1"
```
Consumo hilo: 15, dir: a514700
Consumo hilo: 14, dir: 9d13700
Consumo hilo: 13, dir: 9512700
Consumo hilo: 11, dir: 8d11700
Consumo hilo: 10, dir: fbfff700
Consumo hilo: 12, dir: fb7fe700
El hilo: 11, dir: 8d11700, procesó de la posición 500000 a la 749999
El hilo: 13, dir: 9512700, procesó de la posición 0 a la 249999
El hilo: 10, dir: fbfff700, procesó de la posición 750000 a la 999999
El hilo: 12, dir: fb7fe700, procesó de la posición 250000 a la 499999
El hilo: 15, dir: a514700, procesó de la posición 0 a la 499999
El hilo: 14, dir: 9d13700, procesó de la posición 500000 a la 999999
*** EXITO ***	Tiempo transcurrido: 0.409558. 15 Theads creados hasta el nivel 2.
```

### merge_sort_proc.cpp

Este programa realiza la ordenación de la matriz con 2 procesos de la librería "pthread", es decir, el proceso padre y un subproceso hijo. Cada proceso, genera dos threads de la librería "pthread".  
El tamaño de matriz es definidio por el usuario mediante el parametro row_size, de está forma el tamaño total de la matriz es N=row_size*row_size.  
 - Proceso padre (pocesa de posicion 0 a N/2)  
 - - Thread 1 (procesa de 0 a N/4)  
 - - Thread 2 (procesa de (N/4)+1 a N/2)  
 - Proceso hijo (pocesa de posicion (N/2)+1 a N-1)  
 - - Thread 1 (procesa de (N/2)+1 a (N/2)+(N/4) )   
 - - Thread 2 (procesa de (N/2)+(N/4)+1 a N-1 )  

* se compila con
```
make
```
* se ejecuta con
```
./merge_sort_proc.o row_size if_print
```
#### Parámetros
  - Parámetro row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size  
  - Parámetro if_print (int): Impresíon. Valores; 0 para no imprimir, 2 para procesos y threads utilizados, 2 para imprimir matriz ordenada, 3 para imprimir matriz inicial. Cada número imprime también los datos anteriores

* Salida

Imprime si la matriz fue ordenada correctamente o si dio error, los procesos utilizados, los threads utilizados y el tiempo total que tomó ordenar la matriz, el tiempo es unicamente de lo que le tomó al programa realizar la ordenación

* Ejemplo de la salida ejecutando "./merge_sort_proc.o 1000 1"
```
El hilo dir: f080700, del proceso 0, procesó de la posición 500000 a la 749999
El hilo dir: f080700, del proceso 1, procesó de la posición 0 a la 249999
El hilo dir: e87f700, del proceso 1, procesó de la posición 250000 a la 499999
El hilo dir: e87f700, del proceso 0, procesó de la posición 750000 a la 999999
*** EXITO ***	Tiempo transcurrido: 0.101736. 2 Procesos, cada proceso 2 threads.
```

### merge_sort_cilk.cpp

Este programa realiza la ordenación de la matriz utilizando procesos de la librería "Cilk++", aunque sólo crea sub-procesos hasta el nivel dado por el usuario como parámetro, es decir, la rama izquierda y derecha de cada nivel, son procesadas con un sub-proceso cada una, siempre y cuando el nivel de esas ramas, sea menor al introducido por el usuario.
El tamaño de matriz es definidio por el usuario mediante el parametro row_size, de está forma el tamaño total de la matriz es row_size*row_size.

* se compila con
```
make
```
* se ejecuta con
```
./merge_sort_cilk.o row_size level if_print
```
#### Parámetros
  - Parámetro row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size
  - Parámetro level (int): Nivel hasta el cual se crearán sub-procesos.
  - Parámetro if_print (int): Impresíon. Valores; 0 para no imprimir, 1 imprimir sub-procesos, 2 para imprimir matriz ordenada, 3 para imprimir matriz inicial. Cada número imprime también los datos anteriores

* Salida	

Imprime si la matriz fue ordenada correctamente o si dio error, los procesos utilizados y el tiempo total que tomó ordenar la matriz, el tiempo es unicamente de lo que le tomó al programa realizar la ordenación

* Ejemplo de la salida ejecutando "./merge_sort_cilk.o 1000 3 1"
```
Creación de sub-procesos para nivel 0
Creación de sub-procesos para nivel 1
Creación de sub-procesos para nivel 2
Creación de sub-procesos para nivel 1
Creación de sub-procesos para nivel 2
Creación de sub-procesos para nivel 2
Creación de sub-procesos para nivel 2
*** EXITO ***	Tiempo transcurrido: 0.049840. 42 sub-procesos creados hasta el nivel 3.
```

### merge_sort_tbb.cpp

Lo mismo que el programa "merge_sort_cilk.cpp", pero usando la librería "tbb"

* se compila con
```
make
```
* se ejecuta con
```
./merge_sort_tbb.o row_size level if_print
```
#### Parámetros
  - Parámetro row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size
  - Parámetro level (int): Nivel hasta el cual se crearán sub-procesos.
  - Parámetro if_print (int): Impresíon. Valores; 0 para no imprimir, 1 imprimir sub-procesos, 2 para imprimir matriz ordenada, 3 para imprimir matriz inicial. Cada número imprime también los datos anteriores

* Salida	

Imprime si la matriz fue ordenada correctamente o si dio error, los procesos utilizados y el tiempo total que tomó ordenar la matriz, el tiempo es unicamente de lo que le tomó al programa realizar la ordenación

* Ejemplo de la salida ejecutando "./merge_sort_tbb.o 1000 3 1"
```
Creación de sub-procesos para nivel 2
Creación de sub-procesos para nivel 2
Creación de sub-procesos para nivel 2
Creación de sub-procesos para nivel 2
Creación de sub-procesos para nivel 1
Creación de sub-procesos para nivel 1
Creación de sub-procesos para nivel 0
*** EXITO ***	Tiempo transcurrido: 0.042741. 42 sub-procesos creados hasta el nivel 3.
```

## Resultados

La ejecución de todos los programas se realizó en una computadora de escritorio con las siguientes características
>>>
Architecture:        	x86_64  
CPU op-mode(s):   	32-bit, 64-bit  
Model name:          	Intel(R) Core (TM) i7-6700K CPU @ 4.00GHz, 4001 Mhz,  
4 Core(s),  
8 Logical Processor(s)  
CPU MHz:             	4001  
>>>
### Ejecución para matriz de 1000x1000

El tiempo de ejecución obtenido de los programas para una matriz de 1000x1000 es el siguiente:

|Nivel|Normal|Threads|2 Proc|Cilk|TBB|
| :------: | -------- | -------- | -------- | -------- | -------- |						
|0|0.1472|0.2189|0.0868|0.1630|0.1465|						
|1|0.1472|0.1577|0.0868|0.0838|0.0782|						
|2|0.1472|0.1323|0.0868|0.0535|0.0571|						
|3|0.1472|0.1509|0.0868|0.0432|0.0363|						
|4|0.1472|0.1761|0.0868|0.0451|0.0375|						
|5|0.1472|0.1852|0.0868|0.0470|0.0401|						
|10|0.1472|0.1924|0.0868|0.0439|0.0360|

La primera columna indica el nivel hasta donde se crearon los procesos o threads, esta variable afecta solo a los programas
 - merge_sort_thead.cpp
 - merge_sort_cilk.cpp
 - merge_sort_tbb.cpp  
los otros dos programas merge_sort_normal.cpp y merge_sort_proc.cpp no se ven afectados por el parámetro “nivel”, por esto, el tiempo que les toman resolver una matriz de 1000x1000 es constante.  

**IMPORTANTE** la cantidad de procesos o threads que son consumidos según el nivel máximo hasta donde se pueden crear estos. Al hablar de consumir procesos o threads, es referencia a que se consume uno u el otro, pero no ambos, esto está determinado por el programa, por ejemplo merge_sort_thead.cpp solo consume threads, por otro lado merge_sort_cilk.cpp y merge_sort_tbb.cpp, solo consumen procesos (subprocesos). Entonces, en una matriz de 1000x1000: 
 - Si se introduce 0 como parámetro nivel, entonces solo hasta el nivel 0 se pueden consumir procesos o threads, sin embargo, como es la raíz y el programa ya se está ejecutando, no se consume ningún proceso o thread (ver tercera columna de la siguiente tabla).
 - Si se introduce 1 como parámetro nivel, entonces solo hasta el nivel 1 se pueden consumir procesos o threads. Como el algoritmo subdivide en rama izquierda y rama derecha, entonces cada rama consume un proceso o thread, por eso es por lo que son 2. 
 - Si se introduce 2 como parámetro nivel, entonces solo hasta el nivel 2 se pueden consumir procesos o threads. Si en este nivel hay 4 nodos y en el nivel anterior hay 2, es por ello por lo que se consumen 6 procesos o threads en total.

|Nivel|Hojas del nivel|Procesos / Threads|	
| :------: | :------: | :------: |
|0|1|0|			
|1|2|2|			
|2|4|6|			
|3|8|14|			
|4|16|30|			
|5|32|62|			
|6|64|126|			
|7|128|254|			
|8|256|510|			
|9|512|1022|			
|10|1024|2046|

 Figura ejemplo de merge sort.

 ![alt text](img/merge_sort.png)

 #### Gráfica

 ![alt text](img/graph1000.jpg)
>>>
Etiquetas:
- Normal, se refiere al programa merge_sort_normal.cpp
- Threads, se refiere al programa merge_sort_thread.cpp
- 2 Proc, se refiere al programa merge_sort_proc.cpp
- Cilk, se refiere al programa merge_sort_cilk.cpp
- TBB, se refiere al programa merge_sort_tbb.cpp  
>>>
Tanto el programa merge_sort_normal.cpp como merge_sort_proc.cpp son constantes ya que novarían su ejecución dependiendo del parámetro nivel.  

El programa merge_sort_normal.cpp muestra el tiempo base de comparación, ya que es el programa secuencial, sin usar threads o procesos.  

El programa merge_sort_proc.cpp es casi la mitad de tiempo que merge_sort_normal.cpp, esto se podía anticipar, ya que la rama izquierda y la rama derecha del primer nivel se procesan con subprocesos.  

El programa merge_sort_thread.cpp toma mayor tiempo de procesamiento que el programa “normal”, solo se observa mejora cuando se crean threads hasta el segundo nivel, esto representa el procesamiento de sus primeros 6 nodos con threads. Se puede pensar que esto depende de la computadora donde se ejecute el programa, ya que la computadora donde se ejecutó tiene 8 núcleos. Posterior a este punto se observa un peor tiempo de procesamiento hasta llegar a una estabilización. En la gráfica no se aprecia, pero probando con valores más grandes, eso es lo que sucede.  

Tanto el programa merge_sort_cilk.cpp y merge_sort_tbb.cpp, se comportan de forma similar, observando una pequeña mejora en TBB. A ambos les toma el mismo tiempo de procesamiento cuando no consumen subprocesos, posteriormente el tiempo de procesamiento disminuye a la mitad, observando su mejor rendimiento en el nivel 3 (en este nivel se consumen 14 hilos). Posterior a este nivel, se aprecia un estancamiento.  

### Ejecución para matriz de 5000x5000

El tiempo de ejecución obtenido de los programas para una matriz de 5000x5000 es el siguiente:

|Nivel|Normal|Threads|2 Proc|Cilk|TBB|	
| :------: | -------- | -------- | -------- | -------- | -------- |						
|0|4.0935|5.7032|1.8403|4.4752|4.1027|						
|1|4.0935|3.3260|1.8403|2.3012|2.1473|						
|2|4.0935|1.9753|1.8403|1.2614|1.1831|						
|3|4.0935|1.9331|1.8403|1.0589|0.9748|						
|4|4.0935|3.2559|1.8403|1.0651|0.9704|						
|5|4.0935|3.5446|1.8403|1.0485|0.9687|						
|10|4.0935|3.5866|1.8403|1.0487|0.9800|

#### Gráfica

![alt text](img/graph5000.jpg)

En esta gráfica se aprecia mejor la proporción de mejora que tienen los programas merge_sort_cilk.cpp y merge_sort_tbb.cpp con respecto a merge_sort_normal.cpp  

Otro punto para destacar es la del programa merge_sort_thread.cpp, ya que para matrices de 5000x5000 o mayores, tiene un tiempo de ejecución mejor que merge_sort_normal.cpp  

## Conclusión

- Implementar los conceptos de programación paralela y concurrente ayudan de manera significativa a reducir los tiempos de ejecución. En este caso, se observa una mejora de un 400%.  
- Reducir el tiempo de ejecución para un determinado problema es de extrema importancia ya que al hacerlo se puede ahorrar en los recursos de tiempo y/o potencia de hardware.  
- La proporción de mejora que puede alcanzar un programa desarrollado con procesos esta ligada a la cantidad de procesadores o nodos en donde se ejecuta el programa.

## Autor

* **Antonio Leyva** - [Trabajos](https://gitlab.com/aleyva)

## License

This project is licensed under the GLP License

## Acknowledgments

* Merge Sort
* pthread (fork, nnmap)
* cilk++
* TBB (funciones lambda)
* Mutex 
