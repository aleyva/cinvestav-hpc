//compilacion
//g++ -c utility.cpp;
//g++ -pthread merge_sort_proc.cpp utility.o -o merge_sort_proc.o

#include <stdio.h>
#include <stdlib.h>		// rand, NULL, atoi
#include <chrono>
#include <ctime>
#include <pthread.h>
#include <string.h>
#include <unistd.h> 
#include <sys/types.h> 
#include <sys/wait.h> 
#include <sys/mman.h>
#include "utility.h"

//variables globales
int level_glob;	
int if_print;
int row_size;

//prototipos de funciones
static void *merge_sort(void *args);

int main(int argc, char *argv[]){ 	

	/** Leer el tama�o de la matriz y la variable de impresion, estos numero se introduce como parametro al ejecutar el programa **/	
	if(argc == 3){
		row_size = atoi(argv[1]);
		if_print = atoi(argv[2]); 		
	}else{
		printf("\nDebes introducir 2 parametros;");		
		printf("\n\t- row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size");		
		printf("\n\t- if_print (int): Impresíon. Valores; 0 para no imprimir, 1 imprimir threads, 2 para imprimir matriz ordenada, 3 para imprimir matriz inicial. Cada número imprime también los datos anteriores");
		printf("\n");
		return 1;
	}

	uint32 *mat;
	uint32 memory_mmap_size = (row_size*row_size)*sizeof(uint32);

	mat = (uint32 *)mmap(NULL, memory_mmap_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);			

	mat_init(mat, row_size*row_size);
	if(if_print >= 3){ printf("\nMatriz creada:"); mat_print(mat, row_size);}

	//valores para el proceso padre
	int start =0;
	int end = (row_size*row_size)-1;
	int mid = (start + end) / 2;
	
//variables de reloj
	std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;	

/** iniciar reloj **/	
	start_clock = std::chrono::system_clock::now();	

    //creacion de fork, para rama derecha e izquierda
    pid_t childPID_left_branch = fork(); 			
	//pid_t childPID_left_branch = -1;
    
    if (childPID_left_branch >= 0){ // el fork se creó con exito
     		
		if (childPID_left_branch == 0){ //proceso hijo			
		
			uint32 *mat_temp = (uint32 *)malloc(sizeof(uint32) * (row_size*row_size));
			
			//valores para el proceso hijo rama izquierda									
			int sp_start = start;			
			int sp_end = mid;
			int sp_mid = (sp_start + sp_end) / 2;
			
			//inicializacion y asignacion de parametros para los threads
			pthread_t threads[2];
			struct Args_merge_sort *args_left = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));
			struct Args_merge_sort *args_right = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));
			set_Args_merge_sort(args_left, mat, mat_temp, sp_start, sp_mid, 2);		//asignacion de valores para el hilo izquierdo				
			set_Args_merge_sort(args_right, mat, mat_temp, sp_mid+1, sp_end, 2);		//asignacion de valores para el hilo derecho		
			
			//creacion y union de threads
			if (pthread_create(&threads[0], NULL, &merge_sort, args_left) != 0)
				handle_error_en(1, "pthread_create");

			if (pthread_create(&threads[1], NULL, &merge_sort, args_right) != 0)
				handle_error_en(1, "pthread_create");
			
			if (pthread_join(threads[0], NULL) != 0)
				handle_error_en(1, "pthread_join");
            else
                if(if_print >= 1) printf("\nEl hilo dir: %02x, del proceso 1, procesó de la posición %d a la %d", (unsigned)threads[0], args_left->start, args_left->end);									
			
			if (pthread_join(threads[1], NULL) != 0)
				handle_error_en(1, "pthread_join");
            else
                if(if_print >= 1) printf("\nEl hilo dir: %02x, del proceso 1, procesó de la posición %d a la %d", (unsigned)threads[1], args_right->start, args_right->end);									
			
			// Args_merge_sort *args = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));
			// set_Args_merge_sort(args, mat, mat_temp[0], sp_start, sp_end);	
			// merge_sort(args);

            //merge de los resultados de los dos threads
			merge(mat, mat_temp, sp_start, sp_mid, sp_end);			
			

		}else{	//proceso padre	

			/** merge sort, crea dos hilos, uno para la rama izquierda y otro para la derecha **/			
			uint32 *mat_temp = (uint32 *)malloc(sizeof(uint32) * (row_size*row_size));
			
			//valores para el proceso hijo rama izquierda									
			int sp_start = mid+1;
			int sp_end = end;
			int sp_mid = (sp_start + sp_end) / 2;
			
            //inicializacion y asignacion de parametros para los threads
			pthread_t threads[2];
			struct Args_merge_sort *args_left = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));
			struct Args_merge_sort *args_right = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));
			set_Args_merge_sort(args_left, mat, mat_temp, sp_start, sp_mid, 1);		//asignacion de valores para el hilo izquierdo				
			set_Args_merge_sort(args_right, mat, mat_temp, sp_mid+1, sp_end, 1);		//asignacion de valores para el hilo derecho		
			
			//creacion y union de threads
			if (pthread_create(&threads[0], NULL, &merge_sort, args_left) != 0)
				handle_error_en(1, "pthread_create");

			if (pthread_create(&threads[1], NULL, &merge_sort, args_right) != 0)
				handle_error_en(1, "pthread_create");			
			else
                if(if_print >= 1) printf("\nEl hilo dir: %02x, del proceso 0, procesó de la posición %d a la %d", (unsigned)threads[0], args_left->start, args_left->end);									
			
			if (pthread_join(threads[1], NULL) != 0)
				handle_error_en(1, "pthread_join");
            else
                if(if_print >= 1) printf("\nEl hilo dir: %02x, del proceso 0, procesó de la posición %d a la %d", (unsigned)threads[1], args_right->start, args_right->end);	

			// Args_merge_sort *args = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));
			// set_Args_merge_sort(args, mat, mat_temp[0], sp_start, sp_end);	
			// merge_sort(args);
            
            //merge de los resultados de los dos threads
			merge(mat, mat_temp, sp_start, sp_mid, sp_end);
			
			//espera al proceso hijo a que termine
			wait(NULL);
			
            //merge de los resultados de los dos threads
			merge(mat, mat_temp, start, mid, end);

        /** detener reloj **/	
	        end_clock = std::chrono::system_clock::now();
			
		/** imprimir la matriz **/
			//printf("\n\nProceso izquierdo:"); mat_print(mat, 0, mid);
			//printf("\n\nProceso derecho:"); mat_print(mat, mid+1, end);
			if(if_print == 2){ printf("\nMatriz ordenada:"); mat_print(mat, row_size);}
			
		/** verificacion **/
			if(if_print >= 1) printf("\n");
			mat_is_sort(mat, row_size*row_size);	
		
            //libera segmento de memoria compartida
            munmap(mat, memory_mmap_size);
		
        /** imprimir tiempo **/		
            std::chrono::duration<double> elapsed_seconds = end_clock-start_clock;		
			printf("\tTiempo transcurrido: %f. 2 Procesos, cada proceso 2 threads.\n", elapsed_seconds.count());            
		}

    } else { // fork failed 
        printf("\n Fork failed, EXIT!\n"); 
        return 1; 
    } 
	printf("\n");
   	exit(EXIT_SUCCESS);
}

static void *merge_sort(void *arguments){
	//inicializacion de variables
	struct Args_merge_sort *args = (struct Args_merge_sort *)arguments;		
	int start = args->start;
	int end = args->end;
	
	if (start < end){	

		//declaraciones e inicializaciones de variables
		uint32 *mat = args->mat;
		uint32 *mat_temp = args->mat_temp;
		int level = args->level;
		int mid = (start + end) / 2;				
				
		//inicializacion y asignacion de parametros para los threads
		struct Args_merge_sort *args_left = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));
		struct Args_merge_sort *args_right = (struct Args_merge_sort *)calloc(1, sizeof(struct Args_merge_sort));		
		set_Args_merge_sort(args_left, mat, mat_temp, start, mid, level+1);		//asignacion de valores para el hilo izquierdo				
		set_Args_merge_sort(args_right, mat, mat_temp, mid+1, end, level+1);		//asignacion de valores para el hilo derecho		

		merge_sort(args_left);										
		merge_sort(args_right);								

		merge(mat, mat_temp, start, mid, end);					
	}
}




