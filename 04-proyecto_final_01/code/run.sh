#!/bin/sh
row_size=$1
th_or_lev=$2
if_print=0

if [ $# -eq 2 ]; then
    echo "\n--- merge_sort NORMAL ------------"
    ./merge_sort_normal.o $row_size $if_print;

    echo "\n--- merge_sort THREADS -----------"
    ./merge_sort_thread.o $row_size $th_or_lev $if_print;

    echo "\n--- merge_sort PROCESOS -----------"
    ./merge_sort_proc.o $row_size $if_print;

    echo "\n--- merge_sort CILK -------------"
    ./merge_sort_cilk.o $row_size $th_or_lev $if_print;

    echo "\n--- merge_sort TBB --------------"
    ./merge_sort_tbb.o $row_size $th_or_lev $if_print;
else
    echo "Debes introducir 2 parametros;"
	echo "\t- row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size"
	echo "\t- level (int): Nivel hasta el cual se utilizarán threads o procesos"
    echo "\tEjemplo: ./run.sh 1000 2"
fi

# echo "\n--------------------------------------------"
# echo "------------- merge_sort NORMAL ------------"
# echo "--------------------------------------------"

# ./merge_sort_normal.o $row_size $if_print;

# echo "\n--------------------------------------------"
# echo "------------- merge_sort THREADS -----------"
# echo "--------------------------------------------"
# ./merge_sort_thread.o $row_size $th_or_lev $if_print;

# echo "\n--------------------------------------------"
# echo "------------ merge_sort PROCESOS -----------"
# echo "--------------------------------------------"
# ./merge_sort_proc.o $row_size $if_print;

# echo "\n--------------------------------------------"
# echo "-------------- merge_sort CILK -------------"
# echo "--------------------------------------------"
# ./merge_sort_cilk.o $row_size $th_or_lev $if_print;

# echo "\n--------------------------------------------"
# echo "-------------- merge_sort TBB --------------"
# echo "--------------------------------------------"
# ./merge_sort_tbb.o $row_size $th_or_lev $if_print;