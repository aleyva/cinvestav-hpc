//compilacion
//g++ -c utility.cpp;
//g++ merge_sort_normal.cpp utility.o -o merge_sort_normal.o


#include <stdio.h>
#include <stdlib.h>		// rand, NULL, atoi
#include <chrono>
#include <ctime>
#include "utility.h"

//variables globales
int level_glob;	
int if_print;

//prototipos de funciones
void merge_sort(uint32 *mat, uint32 *mat_temp, int start, int end);

int main(int argc, char *argv[]){ 
	
	//variables
	int row_size;		

	/** Leer el tama�o de la matriz y la variable de impresion, estos numero se introduce como parametro al ejecutar el programa **/	
	if(argc == 3){
		row_size = atoi(argv[1]);
		if_print = atoi(argv[2]); 		
	}else{
		printf("\nDebes introducir 3 parametros;");
		printf("\n\t- row_size (int): Tamaño de fila, el tamaño de la matriz será row_size*row_size");		
		printf("\n\t- if_print (int): Impresíon. Valores; 0 para no imprimir, 2 para imprimir matriz ordenada, 3 para imprimir matriz inicial. Cada número imprime también los datos anteriores");
		printf("\n");
		return 1;
	}

	uint32 *mat = (uint32 *)malloc(sizeof(uint32) * (row_size*row_size));
	uint32 *mat_temp = (uint32 *)malloc(sizeof(uint32) * (row_size*row_size));

	//variables de reloj
	std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;	
	
	//inicializar la matriz
	mat_init(mat, row_size*row_size);
	if(if_print >= 3){ printf("\nMatriz creada:"); mat_print(mat, row_size);}
	
/** iniciar reloj **/	
	start_clock = std::chrono::system_clock::now();	
	
	//ordenar la matriz
	merge_sort(mat, mat_temp, 0, (row_size*row_size)-1);
	
/** detener reloj **/	
	end_clock = std::chrono::system_clock::now();
	
	//imprimir la matriz
	if(if_print >= 2){ printf("\nMatriz ordenada:"); mat_print(mat, row_size);}
	
	//verificacion
	if(if_print >= 1) printf("\n");
	mat_is_sort(mat, row_size*row_size);	
		
	/** imprimir tiempo **/		
	std::chrono::duration<double> elapsed_seconds = end_clock-start_clock;		
	printf("\tTiempo transcurrido: %f\n", elapsed_seconds.count());

	exit(EXIT_SUCCESS);
}

void merge_sort(uint32 *mat, uint32 *mat_temp, int start, int end){
	if (start < end){
		int mid = (start + end) / 2;
		
		merge_sort(mat, mat_temp, start, mid);		
		merge_sort(mat, mat_temp, mid+1, end);		
		
		merge(mat, mat_temp, start, mid, end);
	}else{
		return;
	}
}
