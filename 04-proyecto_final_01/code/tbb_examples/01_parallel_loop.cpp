//compile
//++ -std=c++0x 01_parallel_loop.cpp -ltbb


#include "tbb/tbb.h" 
using namespace tbb; 
#pragma warning( disable: 588) 


void Foo(float a){

}

/*
Igual que el texto de abajo
class ApplyFoo { 
    float *const my_a; 
    public: 
        void operator()( const blocked_range<size_t>& r ) const { 
            float *a = my_a; 
            for( size_t i=r.begin(); i!=r.end(); ++i )  
                Foo(a[i]);      
        } 
        ApplyFoo( float a[] ) : 
            my_a(a) 
        {} 
}; 

void ParallelApplyFoo( float a[], size_t n ) { 
    parallel_for(blocked_range<size_t>(0,n), ApplyFoo(a)); 
} */


void ParallelApplyFoo( float* a, size_t n ) { 
    parallel_for( blocked_range<size_t>(0,n),  
                  [=](const blocked_range<size_t>& r) { 
                      for(size_t i=r.begin(); i!=r.end(); ++i)  
                          Foo(a[i]);  
                  } 
    ); 
} 

int main(){
    return 0;
}