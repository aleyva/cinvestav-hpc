#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <spawn.h>
#include <sys/wait.h>

using namespace std;

int main(int argc,char *argv[], char *envp[])
{
    pid_t ChildProcess;
    pid_t ChildProcess2;
    int RetCode1;
    int RetCode2;
    int Value;
    
    printf("\nUno %d", atoi(argv[1]));
    printf("\nDos %d", atoi(argv[2]));
    //el example1_file2, debe ser un archivo binario, es decir, compilado
    RetCode1 = posix_spawn(&ChildProcess,"example1_file2",NULL,NULL,argv,envp);
    RetCode2 = posix_spawn(&ChildProcess2,"example1_file2",NULL,NULL,argv,envp);
    wait(&Value);
    wait(&Value);
    return(0);
}