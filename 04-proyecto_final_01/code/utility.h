#include <errno.h>

#ifndef UTILITY_H
    #define handle_error_en(en, msg) \
        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

    #define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)

    #define uint32 unsigned long int

    typedef struct Args_merge_sort {    
        uint32 *mat;
        uint32 *mat_temp;
        int start;	
        int end;
        int level;
    } Args_merge_sort;

    //prototipos de funciones
    void mat_init(uint32 *mat, int end);
    void mat_print(uint32 *mat, int row_size);
    void array_print(uint32 *mat, int start, int end);
    int mat_is_sort(uint32 *mat, int end);
    void set_Args_merge_sort(Args_merge_sort *args, uint32 *mat, uint32 *mat_temp, int start, int end, int level);
    void merge(uint32 *mat, uint32 *mat_temp, int start, int mid, int end);

    #define UTILITY_H

#endif /* UTILITY_H */