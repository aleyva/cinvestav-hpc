#include <stdio.h>
#include <stdlib.h>		// rand, NULL, atoi
#include <time.h>
#include "utility.h"

void set_Args_merge_sort(struct Args_merge_sort *args, uint32 *mat, uint32 *mat_temp, int start, int end, int level){
	args->mat = mat;
	args->mat_temp = mat_temp;
	args->start = start;
	args->end = end;
	args->level = level;	
	return;
}

void mat_init(uint32 *mat, int end){
	srand(time(NULL));	
	for(int i = 0; i < end; i++){
		mat[i] = rand()%10000;		
	}
}

void mat_print(uint32 *mat, int row_size){
	printf("\n");
	for(int i = 0; i < row_size; i++){
		for(int j = 0; j < row_size; j++){
			printf("%04lu  ", mat[(i*row_size) + j]);
		}
		printf("\n");
	}	
}

void array_print(uint32 *mat, int start, int end){
	printf("\n");
	for(int i = start; i <= end; i++){		
		printf("%04lu  ", mat[i]);		
	}	
}

int mat_is_sort(uint32 *mat, int end){		
	for(int i = 1; i < end; i++){
		if(mat[i-1]>mat[i]){
			printf("*** Error ***");
			return 1;
		}		
	}	
	printf("*** EXITO ***");
	return 0;
}

void merge(uint32 *mat, uint32 *mat_temp, int start, int mid, int end){
	int n1, n2, i;	
	
	for(n1 = start, n2 = mid+1, i=start; n1 <= mid && n2 <= end; i++){
		if(mat[n1] <= mat[n2])
         mat_temp[i] = mat[n1++];
      else
         mat_temp[i] = mat[n2++];	
	}
   
   while(n1 <= mid)    
      mat_temp[i++] = mat[n1++];

   while(n2 <= end)   
      mat_temp[i++] = mat[n2++];

   for(i = start; i <= end; i++)
      mat[i] = mat_temp[i];
}
