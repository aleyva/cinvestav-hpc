// g++ -pthread program_name.cpp

// CPP Program to multiply two matrix using pthreads 
#include <bits/stdc++.h>
#include <stdio.h>
#include <time.h>
#include <string>
#include <iostream>

using namespace std; 
  
// maximum size of matrix 
#define MAX 1000

// maximum number of threads 
int  MAX_THREAD;

int matA[MAX][MAX]; 
int matB[MAX][MAX]; 
int matC[MAX][MAX]; 
int step_i = 0; 


void* multi(void* arg) 
{ 
    int core = step_i++;
  
    // Each thread computes 1/4th of matrix multiplication 
    for (int i = core * MAX / MAX_THREAD; i < (core + 1) * MAX / MAX_THREAD; i++)  
        for (int j = 0; j < MAX; j++)  
            for (int k = 0; k < MAX; k++)  
                matC[i][j] += matA[i][k] * matB[k][j]; 
} 

void fill_matrixs_rand(){
	// Generating random values in matA and matB 
    for (int i = 0; i < MAX; i++) { 
        for (int j = 0; j < MAX; j++) { 
            matA[i][j] = rand() % 10; 
            matB[i][j] = rand() % 10; 
        } 
    } 
}

void display_matrix(int *mat, char text[]){
	cout << endl 
         << text << endl; 
    for (int i = 0; i < MAX; i++) { 
        for (int j = 0; j < MAX; j++)  
            cout << mat[(i * MAX) + j] << " "; 
        cout << endl; 
    } 
}

void fill_matrixs_with_rand(int *matA,int *matB){
	for (int i = 0; i < MAX; i++) { 
        for (int j = 0; j < MAX; j++) { 
            matA[(i * MAX) + j] = rand() % 10; 
            matB[(i * MAX) + j] = rand() % 10; 
        } 
    } 	
}

int main(int argc, char *argv[]){ 
	 
	if(argc == 2){
		MAX_THREAD = atoi(argv[1]);		
	}else{
		return 1;
	}
	
	//clock variables
	clock_t start_clock, end_clock_creating_threads, end_clock_joining_threads;
	double total_time_creating_t, total_time_joining_t;
	
    // Generating random values in matA and matB 
    fill_matrixs_with_rand(matA[0], matB[0]);
	
    // Displaying matA 
	//display_matrix(matA[0], "Matrix A");

	// Displaying matB 
	//display_matrix(matB[0], "Matrix B");
  
    // declaring four threads 
    pthread_t threads[MAX_THREAD];
	
    start_clock = clock(); 
      
    // Creating four threads, each evaluating its own part 
    for (int i = 0; i < MAX_THREAD; i++) { 
        int* p; 
        pthread_create(&threads[i], NULL, multi, (void*)(p)); 
    } 
    end_clock_creating_threads = clock();

	// joining and waiting for all threads to complete 
    for (int i = 0; i < MAX_THREAD; i++)  
        pthread_join(threads[i], NULL);   
    
	end_clock_joining_threads = clock();

	// Displaying matC ( Multiplication of A and B )
	//display_matrix(matC[0], "Multiplication of A and B");

	//calculating times from start to end
	total_time_creating_t = (double)(end_clock_creating_threads - start_clock) / CLOCKS_PER_SEC;
	total_time_joining_t = (double)(end_clock_joining_threads - start_clock) / CLOCKS_PER_SEC;

	cout << "Thread: " << MAX_THREAD << endl; 
	printf("\tTotal time creating threads: %f\n", total_time_creating_t);
	//cout << "\tTotal time creating threads: " << total_time_creating_t << endl;
	cout << "\tTotal time joining threads: " << total_time_joining_t << endl;
	cout << endl;
	
    return 0; 
}
