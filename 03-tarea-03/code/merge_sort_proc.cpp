//compilacion
// g++ -pthread program_name.cpp

#include <stdio.h>
#include <stdlib.h>		// rand, NULL
#include <pthread.h>
#include <string.h>
#include <time.h>
#include <unistd.h> 
#include <sys/types.h> 
#include <sys/wait.h> 
#include <sys/mman.h>

//las siguiente librerias no estoy seguro de que se ocupen
#include <errno.h>
#include <mutex>

#define handle_error_en(en, msg) \
       do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
       do { perror(msg); exit(EXIT_FAILURE); } while (0)

pthread_mutex_t lock;
int mat_size;
int if_print;

typedef struct Args_thread {    
    int *mat;
	int *mat_temp;
	int start;	
	int end;
} Args_thread;

//prototipos de funciones
void mat_init(int *mat, int end);
void mat_print(int *mat, int mat_size);
void mat_print(int *mat, int start, int end);
int mat_verif(int *mat, int end);
void set_args_thread(Args_thread *args, int *mat, int *mat_temp, int start, int end);
void merge(int *mat, int *mat_temp, int start, int mid, int end);
static void *merge_sort(void *args);

int main(int argc, char *argv[]){ 	

	/** Leer el tama�o de la matriz y la variable de impresion, estos numero se introduce como parametro al ejecutar el programa **/	
	if(argc == 3){
		mat_size = atoi(argv[1]);
		if_print = atoi(argv[2]); 		
	}else{
		printf("\nDebes introducir 2 parametros;");
		printf("\n\t- El tamaño de la matriz (entero de 1 a 1023)");		
		printf("\n\t- variable de impresión de resultados detallados (0 para no imprimir, 1 para imprimir matriz ordenada");
		return 1;
	}

	int *mat;
	int memory_mmap_size = (mat_size*mat_size)*sizeof(int);

	mat = (int *)mmap(NULL, memory_mmap_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);			

	mat_init(mat, mat_size*mat_size);
	//printf("\nMatriz creada:"); mat_print(mat, mat_size);

	//valores para el proceso padre
	int start =0;
	int end = (mat_size*mat_size)-1;
	int mid = (start + end) / 2;
	
//variables de reloj
	clock_t start_clock, end_clock;
	double total_time;
/** iniciar reloj **/	
	start_clock = clock(); 

    //creacion de fork, para rama derecha e izquierda
    pid_t childPID_left_branch = fork(); 			
    
    if (childPID_left_branch >= 0){ // el fork se creó con exito
     		
		if (childPID_left_branch == 0){ //proceso hijo
			
		/** merge sort, crea dos hilos, uno para la rama izquierda y otro para la derecha **/			
			int mat_temp[mat_size][mat_size];
			
			//valores para el proceso hijo rama izquierda									
			int sp_start = start;			
			int sp_end = mid;
			int sp_mid = (sp_start + sp_end) / 2;
			
			//inicializacion y asignacion de parametros para los threads
			pthread_t threads[2];
			struct Args_thread *args_left = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
			struct Args_thread *args_right = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
			set_args_thread(args_left, mat, mat_temp[0], sp_start, sp_mid);		//asignacion de valores para el hilo izquierdo				
			set_args_thread(args_right, mat, mat_temp[0], sp_mid+1, sp_end);		//asignacion de valores para el hilo derecho		
			
			//creacion y union de threads
			if (pthread_create(&threads[0], NULL, &merge_sort, args_left) != 0)
				handle_error_en(1, "pthread_create");

			if (pthread_create(&threads[1], NULL, &merge_sort, args_right) != 0)
				handle_error_en(1, "pthread_create");
			
			if (pthread_join(threads[0], NULL) != 0)
				handle_error_en(1, "pthread_join");
            else
                if(if_print >= 1) printf("\nEl hilo dir: %02x, del proceso 1, procesó de la posición %d a la %d", (unsigned)threads[0], args_left->start, args_left->end);									
			
			if (pthread_join(threads[1], NULL) != 0)
				handle_error_en(1, "pthread_join");
            else
                if(if_print >= 1) printf("\nEl hilo dir: %02x, del proceso 1, procesó de la posición %d a la %d", (unsigned)threads[1], args_right->start, args_right->end);									
			
			// Args_thread *args = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
			// set_args_thread(args, mat, mat_temp[0], sp_start, sp_end);	
			// merge_sort(args);

            //merge de los resultados de los dos threads
			merge(mat, mat_temp[0], sp_start, sp_mid, sp_end);			
			

		}else{	//proceso padre	

			/** merge sort, crea dos hilos, uno para la rama izquierda y otro para la derecha **/			
			int mat_temp[mat_size][mat_size];
			
			//valores para el proceso hijo rama izquierda									
			int sp_start = mid+1;
			int sp_end = end;
			int sp_mid = (sp_start + sp_end) / 2;
			
            //inicializacion y asignacion de parametros para los threads
			pthread_t threads[2];
			struct Args_thread *args_left = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
			struct Args_thread *args_right = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
			set_args_thread(args_left, mat, mat_temp[0], sp_start, sp_mid);		//asignacion de valores para el hilo izquierdo				
			set_args_thread(args_right, mat, mat_temp[0], sp_mid+1, sp_end);		//asignacion de valores para el hilo derecho		
			
			//creacion y union de threads
			if (pthread_create(&threads[0], NULL, &merge_sort, args_left) != 0)
				handle_error_en(1, "pthread_create");

			if (pthread_create(&threads[1], NULL, &merge_sort, args_right) != 0)
				handle_error_en(1, "pthread_create");			
			else
                if(if_print >= 1) printf("\nEl hilo dir: %02x, del proceso 0, procesó de la posición %d a la %d", (unsigned)threads[0], args_left->start, args_left->end);									
			
			if (pthread_join(threads[1], NULL) != 0)
				handle_error_en(1, "pthread_join");
            else
                if(if_print >= 1) printf("\nEl hilo dir: %02x, del proceso 0, procesó de la posición %d a la %d", (unsigned)threads[1], args_right->start, args_right->end);	

			// Args_thread *args = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
			// set_args_thread(args, mat, mat_temp[0], sp_start, sp_end);	
			// merge_sort(args);
            
            //merge de los resultados de los dos threads
			merge(mat, mat_temp[0], sp_start, sp_mid, sp_end);
			
			//espera al proceso hijo a que termine
			wait(NULL);
			
            //merge de los resultados de los dos threads
			merge(mat, mat_temp[0], start, mid, end);

        /** detener reloj **/	
	        end_clock = clock();	
			
		/** imprimir la matriz **/
			//printf("\n\nProceso izquierdo:"); mat_print(mat, 0, mid);
			//printf("\n\nProceso derecho:"); mat_print(mat, mid+1, end);
			if(if_print == 2){ printf("\nMatriz ordenada:"); mat_print(mat, mat_size);}
			
		/** verificacion **/
			if(mat_verif(mat, mat_size*mat_size) == 1)
				printf("\n*********** Error ***********");
			else
				printf("\n*********** Exito ***********");	
		
            //libera segmento de memoria compartida
            munmap(mat, memory_mmap_size);
		
        /** imprimir tiempo **/		
            total_time = (double)(end_clock - start_clock) / CLOCKS_PER_SEC;
            printf("\n\t2 Procesos. 4 Threads, 2 para cada proceso. Total time: %f\n", total_time);
		}

    } else { // fork failed 
        printf("\n Fork failed, EXIT!\n"); 
        return 1; 
    } 
	printf("\n");
   	exit(EXIT_SUCCESS);
}

void set_args_thread(struct Args_thread *args, int *mat, int *mat_temp, int start, int end){
	args->mat = mat;
	args->mat_temp = mat_temp;
	args->start = start;
	args->end = end;	
	return;
}

void mat_init(int *mat, int end){
	srand(time(NULL));	
	for(int i = 0; i < end; i++){
		mat[i] = rand()%10000;
		
	}
}

void mat_print(int *mat, int mat_size){
	printf("\n");
	for(int i = 0; i < mat_size; i++){
		for(int j = 0; j < mat_size; j++){
			printf("%04d  ", mat[(i*mat_size) + j]);
		}
		printf("\n");
	}	
}

void mat_print(int *mat, int start, int end){
	printf("\n");
	for(int i = start; i <= end; i++){		
		printf("%04d  ", mat[i]);		
	}	
}

int mat_verif(int *mat, int end){		
	for(int i = 1; i < end; i++){
		if(mat[i-1] > mat[i]){
			return 1;
		}		
	}
	return 0;
}

void merge(int *mat, int *mat_temp, int start, int mid, int end){
	int n1, n2, i;	
	
	for(n1 = start, n2 = mid+1, i=start; n1 <= mid && n2 <= end; i++){
		if(mat[n1] <= mat[n2])
         mat_temp[i] = mat[n1++];
      else
         mat_temp[i] = mat[n2++];	
	}
   
   while(n1 <= mid)    
      mat_temp[i++] = mat[n1++];

   while(n2 <= end)   
      mat_temp[i++] = mat[n2++];

   for(i = start; i <= end; i++)
      mat[i] = mat_temp[i];
}


static void *merge_sort(void *arguments){
	//inicializacion de variables
	struct Args_thread *args = (struct Args_thread *)arguments;		
	int start = args->start;
	int end = args->end;
	
	if (start < end){	

		//declaraciones e inicializaciones de variables
		int *mat = args->mat;
		int *mat_temp = args->mat_temp;
		int mid = (start + end) / 2;		
				
		//inicializacion y asignacion de parametros para los threads
		struct Args_thread *args_left = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
		struct Args_thread *args_right = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));		
		set_args_thread(args_left, mat, mat_temp, start, mid);		//asignacion de valores para el hilo izquierdo				
		set_args_thread(args_right, mat, mat_temp, mid+1, end);		//asignacion de valores para el hilo derecho		

		merge_sort(args_left);										
		merge_sort(args_right);								

		merge(mat, mat_temp, start, mid, end);					
	}
}




