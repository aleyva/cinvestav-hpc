# Ordenación de una matriz usando merge sort, subprocesos y threads

## merge_sort_proc.cpp

Este programa realiza la ordenación de la matriz mediante el proceso padre y un subproceso hijo. Cada proceso, genera dos threads.  
El tamaño de la matriz es definidio por el usuario mediante parametro, digamos "n". El tamaño total de la matriz es "N = nxn"  
--Proceso padre (pocesa de posicion 0 a N/2)  
  |--Thread 1 (procesa de 0 a N/4)  
  |--Thread 2 (procesa de (N/4)+1 a N/2)  
--Proceso hijo (pocesa de posicion (N/2)+1 a N-1)  
  |--Thread 1 (procesa de (N/2)+1 a (N/2)+(N/4) )   
  |--Thread 2 (procesa de (N/2)+(N/4)+1 a N-1 )  


* se compila con
```
g++ -pthread merge_sort_proc.cpp
```

* se ejecuta con
```
./a.out 1000 1	
```
El primer parametro es el tamaño de la matriz, en este ejemplo la matriz sería de 1000x1000. Entero de entre 1 y 1023.  
El tercer parametro es de impresión. Entero, 0 para no imprimir, 1 para imprimir la matriz de salida.

* Salida

Imprime si la matriz fue ordenada correctamente o si dio error, los procesos utilizados, los threads utilizados y el tiempo total que tomó ordenar la matriz, el tiempo es unicamente de lo que le tomó al programa realizar la ordenación

* Ejemplo de la salida
```
El hilo dir: bc9e1700, procesó de la posición 500000 a la 749999
El hilo dir: bc9e1700, procesó de la posición 0 a la 249999
El hilo dir: bc1e0700, procesó de la posición 250000 a la 499999
El hilo dir: bc1e0700, procesó de la posición 750000 a la 999999
*********** Exito ***********
	2 Procesos. 4 Threads, 2 para cada proceso. Total time: 0.149957
```

## Autor

* **Antonio Leyva** - [Trabajos](https://gitlab.com/aleyva)

## License

This project is licensed under the GLP License

## Acknowledgments

* Merge Sort
* Threads (pthreads)
* fork
* nnmap
