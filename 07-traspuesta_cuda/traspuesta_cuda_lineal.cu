#include <stdio.h>
#include <math.h>

// #define N 1024
// #define NB 2
// #define NT 512

#define N 256   //n² = {1, 4, 9, 16, 25, 36, 49..}
#define NB 4
#define NT 64

__global__ void kernelSumaVectores(float *a, float *c, int n)
{
   int i = threadIdx.x+blockIdx.x*blockDim.x;
   int trans_idx = ((i % n) * n) + (i / n);

   c[trans_idx] = a[i];
   //printf("i:%d - trans:%d - %d*%d + %d - c:%2.0f - a:%2.0f\n", i, trans_idx, (i % n), n, (i / n), c[trans_idx], a[i]);
   
}

void imprimirMatriz(float *array, int n){
   for (int i = 0; i < n; i++){
      for (int j = 0; j < n; j++){      
         printf("%03.0f ", array[i * n + j]);
      }
      printf("\n");
   }
}

void imprimirVector(float *array, int n){
   for (int i = 0; i < n; i++){      
      printf("%03.0f ", array[i]);      
   }
}

void inicializarMatriz(float *array, int n){
   for (int i = 0; i < n; i++){      
      array[i] = i;
   }
}

void trasponerMatrizEnDevice(float *a, float *c, int n)
{
   float *aD, *cD;
   int size = n * sizeof(float);

   cudaSetDevice(0);

   // 1. Asignar memoria
   cudaMalloc(&aD, size);   
   cudaMalloc(&cD, size);

   // 2. Copiar datos del Host al Device
   cudaMemcpy(aD, a, size, cudaMemcpyHostToDevice);   

   // 3. Ejecutar kernel
   kernelSumaVectores<<<NB, NT>>>(aD, cD, (int)ceil(sqrt(N)) );

   // 4. Copiar daros del device al Host
   cudaMemcpy(c, cD, size, cudaMemcpyDeviceToHost);

   // 5. Liberar Memoria
   cudaFree(aD);   
   cudaFree(cD);
}

int main()
{
   float *a, *c;

   a = (float *)malloc(N * sizeof(float));   
   c = (float *)malloc(N * sizeof(float));

   inicializarMatriz(a, N);
   
   printf("Matriz inicial:\n");
   imprimirMatriz(a, (int)ceil(sqrt(N)) );
   //imprimirVector(a, N);

   trasponerMatrizEnDevice(a, c, N);

   printf("Traspuesta:\n");   
   imprimirMatriz(c, (int)ceil(sqrt(N)) );
   // printf("Traspuesta_Vector (primer renglon):\n");
   // imprimirVector(c, (int)ceil(sqrt(N)) );
   //printf("Traspuesta_Vector:\n");
   //imprimirVector(c, N);

   printf("\n");
   free(a);   
   free(c);
   return 0;
}

