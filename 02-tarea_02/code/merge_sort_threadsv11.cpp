//compilacion
// g++ -pthread program_name.cpp

#include <stdio.h>
#include <stdlib.h>		// rand, NULL
#include <time.h>		// seed, rand
#include <pthread.h>
#include <mutex>
#include <errno.h>

#define handle_error_en(en, msg) \
       do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
       do { perror(msg); exit(EXIT_FAILURE); } while (0)

#define uint32 unsigned long int

//variables globales y referentes a los threads
int max_thread;	
int if_print;
struct Thread_info *threads;		//arreglo de threads
pthread_attr_t attr;				//atributos de thread
pthread_mutex_t lock;

struct Thread_info {    		
   pthread_t thread_id;        	/* ID returned by pthread_create() */
   int thread_num;       		/* Application-defined thread # */
   struct Args_thread *args_thread; 
} Thread_info;

typedef struct Args_thread {    
    uint32 *mat;
	uint32 *mat_temp;
	int start;	
	int end;
} Args_thread;

//prototipos de funciones
void mat_init(uint32 *mat, int end);
void mat_print(uint32 *mat, int mat_size);
void array_print(uint32 *mat, int start, int end);
int mat_is_sort(uint32 *mat, int end);
void set_args_thread(Args_thread *args, uint32 *mat, uint32 *mat_temp, int start, int end);
void pthread_create_merge_sort(int num_thread);
void pthread_join_merge_sort(int num_left_thread);
void merge(uint32 *mat, uint32 *mat_temp, int start, int mid, int end);
static void *merge_sort(void *args);


int main(int argc, char *argv[]){ 	
	
	//variables
	int mat_size;			
	int return_value;					//varible para controlar los codigos de error	
	int max_thread_arg;
	
	//variables de reloj
	clock_t start_clock, end_clock;
	double total_time;
	
/** Leer parametros; tamaño de la matriz, numero de threads con que se va a trabajar y variable para impresion **/	
	if(argc == 4){		
		mat_size = atoi(argv[1]);
		max_thread_arg = atoi(argv[2]);
		if_print = atoi(argv[3]); 
	}else{
		printf("\nDebes introducir 3 parametros;");
		printf("\n\t- El tamaño de la matriz (entero de 1 a 1023)");
		printf("\n\t- el #threads con que se va a trabajar (entero de 1 a N)");
		printf("\n\t- variable de impresión de resultados detallados (0 para no imprimir, 1 para imprimir hilos, 2 para imprimir hilos y matriz de salida)");
		return 1;
	}
	uint32 *mat = (uint32 *)malloc(sizeof(uint32) * (mat_size*mat_size));
	uint32 *mat_temp = (uint32 *)malloc(sizeof(uint32) * (mat_size*mat_size));
	
	
/** Inicializacion del entorno para trabajar con threads **/	

	//variable para manejar los argumentos de los threads
	struct Args_thread *args_thread;	//parametros de thread	

	//asignacion de variables para max_thread	
	max_thread = max_thread_arg;		

	//reserva de memoria para threads
	threads = (struct Thread_info *)calloc(max_thread, sizeof(struct Thread_info));	
	max_thread--;
   	if (threads == NULL)
       handle_error("calloc threads");
       
    //inicializacion attr, los atributos de los threads, los atributos son los de default
	return_value = pthread_attr_init(&attr);
	if (return_value != 0)
	   handle_error_en(return_value, "pthread_attr_init");
	
	//reserva de memoria para args_thread, argumentos del thread	
	args_thread = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
	if (args_thread == NULL)
       handle_error("calloc args_thread");
       
    //asignación de numero de thread (esto es diferente de id de thread)
    for (int tnum = max_thread; tnum >= 0; tnum--) {
       threads[tnum].thread_num = tnum;
   	}
/** Fin de inicializacion de entorno para trabajar con threads **/
	

/** inicializar e imprimir la matriz **/
	mat_init(mat, mat_size*mat_size);	
	//printf("\nMatriz creada:"); mat_print(mat[0], mat_size);	

/** iniciar reloj **/	
	start_clock = clock(); 
	
/** ordenar la matriz **/
	//establecer los valores a args_thread	
	set_args_thread(args_thread, mat, mat_temp, 0, (mat_size*mat_size)-1);
	merge_sort(args_thread);

/** detener reloj **/	
	end_clock = clock();
	
/** imprimir la matriz **/
	if(if_print >= 2){ printf("\nMatriz ordenada:"); mat_print(mat, mat_size);}
	
/** verificacion **/
	if(mat_is_sort(mat, mat_size*mat_size) == 1)
		printf("\n*********** Error ***********");
	else
		printf("\n*********** Exito ***********");	
		
/** imprimir tiempo **/		
	total_time = (double)(end_clock - start_clock) / CLOCKS_PER_SEC;
	printf("\n\tThreads: %d, Total time: %f\n", max_thread_arg, total_time);
	
/** liberar memoria **/			
	return_value = pthread_attr_destroy(&attr);
   	if (return_value != 0)
      	handle_error_en(return_value, "pthread_attr_destroy");
    
    free(threads);
   	exit(EXIT_SUCCESS);
}

void set_args_thread(struct Args_thread *args, uint32 *mat, uint32 *mat_temp, int start, int end){
	args->mat = mat;
	args->mat_temp = mat_temp;
	args->start = start;
	args->end = end;	
	return;
}

void mat_init(uint32 *mat, int end){
	srand(time(NULL));	
	for(int i = 0; i < end; i++){
		mat[i] = rand()%10000;
		
	}
}

void mat_print(uint32 *mat, int mat_size){
	printf("\n");
	for(int i = 0; i < mat_size; i++){
		for(int j = 0; j < mat_size; j++){
			printf("%04lu  ", mat[(i*mat_size) + j]);
		}
		printf("\n");
	}	
}

void array_print(uint32 *mat, int start, int end){
	printf("\n");
	for(int i = start; i <= end; i++){		
		printf("%04lu  ", mat[i]);		
	}	
}

int mat_is_sort(uint32 *mat, int end){		
	for(int i = 1; i < end; i++){
		if(mat[i-1]>mat[i]){
			return 1;
		}		
	}
	return 0;
}

void pthread_create_merge_sort(int num_thread){
	pthread_mutex_lock(&lock);	
		int return_value = pthread_create(&threads[num_thread].thread_id, &attr, &merge_sort, threads[num_thread].args_thread);	
	pthread_mutex_unlock(&lock);
		if (return_value != 0)
			handle_error_en(return_value, "pthread_create");
		else
			if(if_print >= 1) printf("\nConsumo hilo: %d, dir: %02x", num_thread, (unsigned)threads[num_thread].thread_id);		
}

void pthread_join_merge_sort(int num_thread){	
	int return_value = pthread_join(threads[num_thread].thread_id, NULL);
	if (return_value != 0)
		handle_error_en(return_value, "pthread_join");
	else			
		if(if_print >= 1) printf("\nEl hilo: %d, dir: %02x, procesó de la posición %d a la %d", num_thread, (unsigned)threads[num_thread].thread_id, threads[num_thread].args_thread->start, threads[num_thread].args_thread->end);									
}

void merge(uint32 *mat, uint32 *mat_temp, int start, int mid, int end){
	int n1, n2, i;	
	
	for(n1 = start, n2 = mid+1, i=start; n1 <= mid && n2 <= end; i++){
		if(mat[n1] <= mat[n2])
         mat_temp[i] = mat[n1++];
      else
         mat_temp[i] = mat[n2++];	
	}
   
   while(n1 <= mid)    
      mat_temp[i++] = mat[n1++];

   while(n2 <= end)   
      mat_temp[i++] = mat[n2++];

   for(i = start; i <= end; i++)
      mat[i] = mat_temp[i];
}


static void *merge_sort(void *arguments){
	//inicializacion de variables
	struct Args_thread *args = (struct Args_thread *)arguments;		
	int start = args->start;
	int end = args->end;
	
	if (start < end){		

		//declaraciones e inicializacion de variables
		uint32 *mat = args->mat;
		uint32 *mat_temp = args->mat_temp;
		int mid = (start + end) / 2;
		int num_left_thread = -1;
		int num_right_thread = -1;
		int return_value;
				
		//inicializacion y asignacion de parametros para los threads
		struct Args_thread *args_left = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
		struct Args_thread *args_right = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
		set_args_thread(args_left, mat, mat_temp, start, mid);		//asignacion de valores para el hilo izquierdo				
		set_args_thread(args_right, mat, mat_temp, mid+1, end);		//asignacion de valores para el hilo derecho		

		pthread_mutex_lock(&lock);
			//¿aun se puede consumir un thread? rama izquierda
			if(max_thread >= 0){
				num_left_thread = max_thread;
				max_thread--;
				threads[num_left_thread].args_thread = args_left;	//parametros thread		
			}
			
			//¿aun se puede consumir un thread? rama derecha
			if(max_thread >= 0){
				num_right_thread = max_thread;
				max_thread--;
				threads[num_right_thread].args_thread = args_right;	//parametros thread		
			}
		pthread_mutex_unlock(&lock);
		
		//si se reservo un numero de thread para num_left_thread, la rama izquierda se procesa con thread
		if(num_left_thread != -1){
			pthread_create_merge_sort(num_left_thread);
		}else{
			merge_sort(args_left);								
		}
		
		//si se reservo un numero de thread para num_right_thread, la rama derecha se procesa con thread
		if(num_right_thread != -1){
			pthread_create_merge_sort(num_right_thread);
		}else{
			merge_sort(args_right);								
		}
			
		//si se reservo un numero de thread para num_left_thread, se hace join de su thread
		if(num_left_thread != -1){		    		
			pthread_join_merge_sort(num_left_thread);				
		}	
		
		//si se reservo un numero de thread para num_right_thread, se hace join de su thread		
		if(num_right_thread != -1){		    		
    		pthread_join_merge_sort(num_right_thread);			
		}	

		//pthread_mutex_lock(&lock);
			merge(mat, mat_temp, start, mid, end);			
		//pthread_mutex_unlock(&lock);
		
	}
}




