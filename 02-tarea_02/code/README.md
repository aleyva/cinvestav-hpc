
# Ordenación de una matriz, utilizando merge sort y threads

Todos los archivos son programas en c++, utilizan el algoritmo merge sort para ordenar una matriz de NxN  
La matriz es generada con numeros aleatorios.


## merge_sort_normal.cpp

Este programa realiza la ordenación de una matriz de 1000x1000 de forma secuencial.

* se compila con
```
g++ merge_sort_normal.cpp;
```
* se ejecuta con
```
./a.out
```
* Salida
	
Imprime si la matriz fue ordenada correctamente o si dio error, ademas del tiempo total que tomó ordenar la matriz, el tiempo es unicamente de lo que le tomó al programa realizar la ordenación

* Ejemplo de la salida
```
*********** Exito ***********
	Total time: 0.306397
```

## merge_sort_threadsv4.cpp

Este programa realiza la ordenación de una matriz de 1000x1000 utilizando threads, aunque solo usa threads para la ordenación en la rama izquierda, la rama derecha la procesa de forma secuencial.

* se compila con
```
g++ -pthread merge_sort_threadsv4.cpp
```
* se ejecuta con
```
./a.out 3
```
Donde el parametro indica el numero de threads a utilizar por el programa.

* Salida

Imprime los hilos consumidos e imprime la pocision que procesaron de la matriz (la matriz se ve de forma lineal), es decir, como si fuerra vector o arreglo unidimensional  	
Imprime si la matriz fue ordenada correctamente o si dio error, ademas del tiempo total que tomó ordenar la matriz, el tiempo es unicamente de lo que le tomó al programa realizar la ordenación

* Ejemplo de la salida
```
Consumo hilo "izquierdo" 1
Consumo hilo "izquierdo" 0
El hilo "izquierdo" 0, proceso de la posicion 500000 a la 749999
El hilo "izquierdo" 1, proceso de la posicion 0 a la 499999
*********** Exito ***********
	Threads: 2, Total time: 0.138892
```

### merge_sort_threadsv11.cpp

Este programa procesa cada rama de merge_sort con threads. Esto lo realiza hasta que se acaban los threads dados por el usuario (dado por parametro al ejecutar el programa)

* se compila con
```
g++ -pthread merge_sort_threadsv11.cpp
```

* se ejecuta con
```
./a.out 1000 4 1	
```
El primer parametro es el tamaño de la matriz, en este ejemplo la matriz sería de 1000x1000. Entero de entre 1 y 1023.  
El segundo parametro es el numero de threads que utilizará el programa. Entero de 0 a N.  
El tercer parametro es de impresión. Entero, 0 para no imprimir, 1 para imprimir hilos, 2 para imprimir hilos y matriz de salida.  

* Salida

Imprime si la matriz fue ordenada correctamente o si dio error, los threads utilizados y el tiempo total que tomó ordenar la matriz, el tiempo es unicamente de lo que le tomó al programa realizar la ordenación

* Ejemplo de la salida
```
Consumo hilo: 3, dir: 3cc6d700
Consumo hilo: 2, dir: 37fff700
Consumo hilo: 1, dir: 377fe700
Consumo hilo: 0, dir: 36ffd700
El hilo: 1, dir: 377fe700, procesó de la posición 0 a la 249999
El hilo: 0, dir: 36ffd700, procesó de la posición 250000 a la 499999
El hilo: 3, dir: 3cc6d700, procesó de la posición 0 a la 499999
El hilo: 2, dir: 37fff700, procesó de la posición 500000 a la 999999
*********** Exito ***********
	Threads: 4, Total time: 0.717891
```

## Autor

* **Antonio Leyva** - *Initial work* - [Trabajos](https://gitlab.com/aleyva)

## License

This project is licensed under the GLP License

## Acknowledgments

* Merge Sort
* Threads (pthreads)
* Mutex 
