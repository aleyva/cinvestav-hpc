#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//prototipos de funciones
void mat_init(int *mat, int end);
void mat_print(int *mat, int sizeMat);
int mat_verif(int *mat, int end);
void merge(int *mat, int *mat_temp, int start, int mid, int end);
void merge_sort(int *mat, int *mat_temp, int start, int end);
	


int main(){
	
	//variables
	int sizeMat = 1000;
	int mat[sizeMat][sizeMat];
	int mat_temp[sizeMat][sizeMat];
	int max_thread = 10;
	
	//variables de reloj
	clock_t start_clock, end_clock;
	double total_time;
	
	//inicializar la matriz
	mat_init(mat[0], sizeMat*sizeMat);
	
/** iniciar reloj **/	
	start_clock = clock(); 	
	
	//ordenar la matriz
	merge_sort(mat[0], mat_temp[0], 0, (sizeMat*sizeMat)-1);
	
/** detener reloj **/	
	end_clock = clock();
	
	//imprimir la matriz
	//mat_print(mat[0], sizeMat);
	
	//verificacion
	if(mat_verif(mat[0], sizeMat) == 1)
		printf("\n*********** Error ***********");
	else
		printf("\n*********** Exito ***********");	
		
	/** imprimir tiempo **/		
	total_time = (double)(end_clock - start_clock) / CLOCKS_PER_SEC;
	printf("\n\tTotal time: %f\n", total_time);
}

void mat_init(int *mat, int end){
	srand(NULL);	
	for(int i = 0; i < end; i++){
		mat[i] = rand()%10000;
		//printf("\n%d", i);
	}
}

void mat_print(int *mat, int sizeMat){
	for(int i = 0; i < sizeMat; i++){
		for(int j = 0; j < sizeMat; j++){
			printf("%04d  ", mat[(i*sizeMat) + j]);
		}
		printf("\n");
	}
}

int mat_verif(int *mat, int end){		
	for(int i = 1; i < end; i++){
		if(mat[i-1]>mat[i]){
			return 1;
		}		
	}
	return 0;
}

void merge(int *mat, int *mat_temp, int start, int mid, int end){
	int n1, n2, i;	
	
	for(n1 = start, n2 = mid+1, i=start; n1 <= mid && n2 <= end; i++){
		if(mat[n1] <= mat[n2])
         mat_temp[i] = mat[n1++];
      else
         mat_temp[i] = mat[n2++];	
	}
   
   while(n1 <= mid)    
      mat_temp[i++] = mat[n1++];

   while(n2 <= end)   
      mat_temp[i++] = mat[n2++];

   for(i = start; i <= end; i++)
      mat[i] = mat_temp[i];
}

void merge_sort(int *mat, int *mat_temp, int start, int end){
	if (start < end){
		int mid = (start + end) / 2;
		
		//int* p; 
        //pthread_create(&threads[i], NULL, multi, (void*)(p)); 
		//ejecutar en un hilo
		merge_sort(mat, mat_temp, start, mid);
		//ejecutar en un hilo
		merge_sort(mat, mat_temp, mid+1, end);
		
		//pthread_join(threads[i], NULL); 
		merge(mat, mat_temp, start, mid, end);
	}else{
		return;
	}
}
