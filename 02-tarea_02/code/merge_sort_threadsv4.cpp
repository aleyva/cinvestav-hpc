//compilacion
// g++ -pthread program_name.cpp


#include <stdio.h>
#include <stdlib.h>		// rand, NULL
#include <pthread.h>
#include <time.h>
//las siguiente librerias no estoy seguro de que se ocupen
#include <stdlib.h>
#include <errno.h>

#define handle_error_en(en, msg) \
       do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
       do { perror(msg); exit(EXIT_FAILURE); } while (0)

//variables globales y referentes a los threads
int max_thread;	
struct Thread_info *threads;		//arreglo de threads
pthread_attr_t attr;			//atributos de thread

struct Thread_info {    		/* Used as argument to thread_start() */
   pthread_t thread_id;        	/* ID returned by pthread_create() */
   int thread_num;       		/* Application-defined thread # */
   struct Args_thread *args_thread; 
} Thread_info;

typedef struct Args_thread {    
    int *mat;
	int *mat_temp;
	int start;	
	int end;
} Args_thread;

//prototipos de funciones
void mat_init(int *mat, int end);
void mat_print(int *mat, int sizeMat);
int mat_verif(int *mat, int end);
void set_args_thread(Args_thread *args, int *mat, int *mat_temp, int start, int end);
void merge(int *mat, int *mat_temp, int start, int mid, int end);
static void *merge_sort(void *args);


int main(int argc, char *argv[]){ 
	
	//variables
	int sizeMat = 1000;
	int mat[sizeMat][sizeMat];
	int mat_temp[sizeMat][sizeMat];		
	int return_value;					//varible para controlar los codigos de error	
	int max_thread_arg;
	
	//variables de reloj
	clock_t start_clock, end_clock;
	double total_time;
	
/** Leer numero de threads con que se va a trabajar, este numero se introduce como parametro al ejecutar el programa **/	
	if(argc == 2){
		max_thread_arg = atoi(argv[1]);		
	}else{
		printf("\n Debes introducir el numero de threads con que se va a trabajar, como parametro");
		return 1;
	}	
	
/** Inicializacion del entorno para trabajar con threads **/	

	//variables de los thread				
	struct Args_thread *args_thread;	//parametros de thread
	//unsigned long int stack_size = 0x100000;			//stack size

	//asignacion de variables para max_thread	
	max_thread = max_thread_arg;	
	
	//reserva de memoria para threads, el arreglo de threads
	threads = (struct Thread_info *)calloc(max_thread, sizeof(struct Thread_info));
	max_thread--;
   	if (threads == NULL)
       handle_error("calloc threads");
       
    //inicializacion attr, los atributos de los threads, los atributos son los de default
	return_value = pthread_attr_init(&attr);
	if (return_value != 0)
	   handle_error_en(return_value, "pthread_attr_init");
	   
	/* cambiar el stack_size para los threads *
    return_value = pthread_attr_setstacksize(&attr, stack_size);
    if (return_value != 0)
    	handle_error_en(return_value, "pthread_attr_setstacksize"); 
    */
	
	//reserva de memoria para args_thread, argumentos del thread	
	args_thread = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
	if (args_thread == NULL)
       handle_error("calloc args_thread");
       
    //asignaci�n de numero de thread (esto es diferente de id de thread)
    for (int tnum = max_thread; tnum >= 0; tnum--) {
       threads[tnum].thread_num = tnum;
   	}

/** Fin de inicializacion de entorno para trabajar con threads **/
	
	
/** inicializar e imprimir la matriz **/
	mat_init(mat[0], sizeMat*sizeMat);	
	//printf("Matriz creada:\n"); mat_print(mat[0], sizeMat);	

/** iniciar reloj **/	
	start_clock = clock(); 
	
/** ordenar la matriz **/
	//establecer los valores a args_thread	
	set_args_thread(args_thread, mat[0], mat_temp[0], 0, (sizeMat*sizeMat)-1);
	merge_sort(args_thread);

/** detener reloj **/	
	end_clock = clock();
	
/** imprimir la matriz **/
	//printf("Matriz ordenada:\n"); mat_print(mat[0], sizeMat);
	
/** verificacion **/
	if(mat_verif(mat[0], sizeMat) == 1)
		printf("\n*********** Error ***********");
	else
		printf("\n*********** Exito ***********");	
		
/** imprimir tiempo **/		
	total_time = (double)(end_clock - start_clock) / CLOCKS_PER_SEC;
	printf("\n\tThreads: %d, Total time: %f\n", max_thread_arg, total_time);
	
/** liberar memoria **/			
	return_value = pthread_attr_destroy(&attr);
   	if (return_value != 0)
      	handle_error_en(return_value, "pthread_attr_destroy");
    
    free(threads);
   	exit(EXIT_SUCCESS);
}

void set_args_thread(struct Args_thread *args, int *mat, int *mat_temp, int start, int end){
	args->mat = mat;
	args->mat_temp = mat_temp;
	args->start = start;
	args->end = end;	
	return;
}

void mat_init(int *mat, int end){
	srand(NULL);	
	for(int i = 0; i < end; i++){
		mat[i] = rand()%10000;
		
	}
}

void mat_print(int *mat, int sizeMat){
	for(int i = 0; i < sizeMat; i++){
		for(int j = 0; j < sizeMat; j++){
			printf("%04d  ", mat[(i*sizeMat) + j]);
		}
		printf("\n");
	}
	printf("\n");
}

int mat_verif(int *mat, int end){		
	for(int i = 1; i < end; i++){
		if(mat[i-1]>mat[i]){
			return 1;
		}		
	}
	return 0;
}

void merge(int *mat, int *mat_temp, int start, int mid, int end){
	int n1, n2, i;	
	
	for(n1 = start, n2 = mid+1, i=start; n1 <= mid && n2 <= end; i++){
		if(mat[n1] <= mat[n2])
         mat_temp[i] = mat[n1++];
      else
         mat_temp[i] = mat[n2++];	
	}
   
   while(n1 <= mid)    
      mat_temp[i++] = mat[n1++];

   while(n2 <= end)   
      mat_temp[i++] = mat[n2++];

   for(i = start; i <= end; i++)
      mat[i] = mat_temp[i];
}

//void merge_sort(int *mat, int *mat_temp, int start, int end){
static void *merge_sort(void *arguments){
	//inicializacion de variables
	struct Args_thread *args = (struct Args_thread *)arguments;		
	int start = args->start;
	int end = args->end;
	
	if (start < end){		

		//declaraciones e inicializaciones de variables
		int *mat = args->mat;
		int *mat_temp = args->mat_temp;
		int mid = (start + end) / 2;
		int num_left_thread = -1;
		int num_right_thread = -1;
		int return_value;
				
		//inicializacion y asignacion de parametros para los threads
		Args_thread *args_left, *args_right;			
		args_left = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));
		args_right = (struct Args_thread *)calloc(1, sizeof(struct Args_thread));												
		set_args_thread(args_left, mat, mat_temp, start, mid);		//asignacion de valores para el hilo izquierdo				
		set_args_thread(args_right, mat, mat_temp, mid+1, end);		//asignacion de valores para el hilo derecho
				
		//�aun se puede consumir un thread? lado izquierdo (verificar la condicion de carrera)
		if(max_thread >= 0){
			num_left_thread = max_thread;
			max_thread--;
		}

		//�aun se puede consumir un thread? lado derecho (verificar la condicion de carrera)
		if(max_thread >= 0){
			//num_right_thread = max_thread;
			//max_thread--;
		}
		
		//si se reservo un numero de thread para num_left_thread, la parte izquierda del arreglo se procesa con thread
		if(num_left_thread != -1){
			//asignacion de los valores del thread			     
			threads[num_left_thread].args_thread = args_left;	//parametros thread
		
			//creacion y ejecucion de thread
		    return_value = pthread_create(&threads[num_left_thread].thread_id, &attr, &merge_sort, &threads[num_left_thread].args_thread);		    
			if (return_value != 0)
				handle_error_en(return_value, "pthread_create");
			else
				printf("\nConsumo hilo \"izquierdo\" %d", num_left_thread);
		}else{
			merge_sort(args_left);								
		}
		
		if(num_right_thread != -1){
			//asignacion de los valores del thread			     
			threads[num_right_thread].args_thread = args_right;	//parametros thread
		
			//creacion y ejecucion de thread
		    return_value = pthread_create(&threads[num_right_thread].thread_id, &attr, &merge_sort, &threads[num_right_thread].args_thread);		    
			if (return_value != 0)
				handle_error_en(return_value, "pthread_create");
			else
				printf("\nConsumo hilo \"derecho\" %d", num_right_thread);
		}else{
			merge_sort(args_right);								
		}
			
		//si se reservo un numero de thread para num_left_thread, la parte izquierda del arreglo se procesa con thread
		if(num_left_thread != -1){		    		
    		return_value = pthread_join(threads[num_left_thread].thread_id, NULL);
       		if (return_value != 0)
           		handle_error_en(return_value, "pthread_join");
        	else
        		printf("\nEl hilo \"izquierdo\" %d, proceso de la posicion %d a la %d", threads[num_left_thread].thread_num, threads[num_left_thread].args_thread->start, threads[num_left_thread].args_thread->end);
		}	
		
		//si se reservo un numero de thread para num_right_thread, la parte izquierda del arreglo se procesa con thread
		if(num_right_thread != -1){		    		
    		return_value = pthread_join(threads[num_right_thread].thread_id, NULL);
       		if (return_value != 0)
           		handle_error_en(return_value, "pthread_join");
        	else
        		printf("\nEl hilo \"derecho\" %d, proceso de la posicion %d a la %d", threads[num_right_thread].thread_num, threads[num_right_thread].args_thread->start, threads[num_right_thread].args_thread->end);
		}
		
		merge(mat, mat_temp, start, mid, end);
	}
}


