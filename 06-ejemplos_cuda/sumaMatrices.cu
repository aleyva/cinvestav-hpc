#include <stdio.h>
#include <stdlib.h>

#define M 10240
#define N 10240

//double a[M][N], b[M][N], c[M][N];
double *a, *b, *c;

__global__ void kernelSumaMatrices(double *a, double *b, int m, int n)
{
   int i = threadIdx.x + blockIdx.x*blockDim.x;
   int j = threadIdx.y + blockIdx.y*blockDim.y;

//   c[i][j] = a[i][j] + b[i][j];
   while (i < m) {
       j = threadIdx.y + blockIdx.y*blockDim.y;
       while (j < n) {
           a[i*n+j] += b[i*n+j];
           j += blockDim.y*gridDim.y;
       }
       i+=blockDim.x*gridDim.x;
   }
}

void sumaMatricesEnDevice(double *a, double *b, double *c, int m, int n) {
    double *aD, *bD;
    int size=m*n*sizeof(double);
    dim3 bloques(4,4);
    dim3 hilos(32,32);
    cudaSetDevice(2);

    // 1. Asignar memoria 
    cudaMalloc(&aD, size); 
    cudaMalloc(&bD, size); 

   // 2. Copiar datos del Host al Device 
   cudaMemcpy(aD, a, size, cudaMemcpyHostToDevice);    
   cudaMemcpy(bD, b, size, cudaMemcpyHostToDevice);    

   // 3. Ejecutar kernel 
   kernelSumaMatrices<<<bloques , hilos>>>(aD, bD, m, n);

   // 4. Copiar datos del device al Host
   cudaMemcpy(c, aD, size, cudaMemcpyDeviceToHost);

   // 5. Liberar Memoria
   cudaFree(aD); cudaFree(bD);
}

int main() {
   int i, j;

   a=(double *)malloc(M*N*sizeof(double));
   b=(double *)malloc(M*N*sizeof(double));
   c=(double *)malloc(M*N*sizeof(double));

  for (i=0; i<M; i++) {
      for (j=0; j<N; j++) {
         a[i*N+j] = b[i*N+j] = i+j;
      }
   }

  sumaMatricesEnDevice(a, b, c, M, N);

   for (i=M-1; i<M; i++) {
      for (j=0; j<N; j++) {
         printf("%5.2f ", c[i*N+j]);
      }
      printf("\n");
   }

   free(a); free(b); free(c);
}


