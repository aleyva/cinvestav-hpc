#include <stdio.h>
#include <stdlib.h>

#define N 1024

double *a, *b, *c; 

__global__ void kernelMultiplicaMatrices(double *a, double *b, double *c, int n) {
    int i = threadIdx.x+blockDim.x*blockIdx.x;
    int j = threadIdx.y+blockDim.y*blockIdx.y;
    int k;
    double tmp=0.0;
    double rb, rc;
    
    for (k=0; k<n; k++) {
        rb = b[i*n+k];
        rc = c[k*n+j];
        tmp += rb*rc;
       // a[i*n+j] = b[i*n+k]*c[k*n+j]; 
    }
    a[i*n+j] = tmp;
    
}

__global__ void kernelMultiplicaMatrices_1(double *a, double *b,
                                         double *c, int n) {
    int i = threadIdx.x+blockDim.x*blockIdx.x;
    int j = threadIdx.y+blockDim.y*blockIdx.y;
    int x = threadIdx.x;
    int y = threadIdx.y;
    int jl, il; 
    int k, l, ll;
    __shared__ double A[32][32], B[32][32], C[32][32];
    
    l = n/blockDim.x; 

    A[x][y] = 0.0; 
    __syncthreads();

    for (ll=0; ll<l; ll++)   { 
       jl = y+ll*blockDim.y;
       il = x+ll*blockDim.x;  
       B[x][y] = b[i*n+jl];
       C[x][y] = c[il*n+j];
       __syncthreads();
    
    for (k=0; k<32; k++) {
        A[x][y] += B[x][k]*C[k][y];
     }
  }
   
    a[i*n+j] = A[x][y];
    
}

float multiplicaMatricesEnDevice(double *a, double *b, double *c, int n, int kl) {
    double *aD, *bD, *cD; 
    int size=n*n*sizeof(double); 
    cudaEvent_t start, stop; 
    float t; 
    dim3 bloques(32,32); 
    dim3 hilos(32, 32); 
    
    cudaEventCreate(&start); cudaEventCreate(&stop); 
    cudaEventRecord(start, 0);
    cudaMalloc(&aD, size); cudaMalloc(&bD, size); cudaMalloc(&cD, size);

    cudaMemcpy(bD, b, size, cudaMemcpyDefault); 
    cudaMemcpy(cD, c, size, cudaMemcpyDefault);

    if (kl==0)
       kernelMultiplicaMatrices<<<bloques, hilos>>>(aD, bD, cD, n);
    else 
       kernelMultiplicaMatrices_1<<<bloques, hilos>>>(aD, bD, cD, n);


    cudaMemcpy(a, aD, size, cudaMemcpyDefault);


    cudaFree(aD); cudaFree(bD); cudaFree(cD); 
    cudaEventRecord(stop, 0); 
    cudaEventSynchronize(stop);


    cudaEventElapsedTime(&t, start, stop); 
    cudaEventDestroy(start); cudaEventDestroy(stop); 
    return t; 
}


int main(int argc, char **argv) {
   int d=0, kl=0; 
   int size = N*N*sizeof(double); 
   int i, j; 
   float t; 

   if (argc > 1)
     d = atoi(argv[1]); 
 
   if (argc > 2) 
     kl = atoi(argv[2]);     

   printf("Multiplica matrices en device %d, kernel %d\n", d, kl);

   cudaSetDevice(d%3); 
   cudaHostAlloc(&a, size, cudaHostAllocDefault);      
   cudaHostAlloc(&b, size, cudaHostAllocDefault);      
   cudaHostAlloc(&c, size, cudaHostAllocDefault); 

   for (i=0; i<N; i++) { 
      for (j=0; j<N; j++) { 
         b[i*N+j] = i+j; 
         c[i*N+j] = (i==j)?2.0:0.0; 
      }
   }

   t = multiplicaMatricesEnDevice(a, b, c, N, kl); 

   for (i=N; i<N; i++) {
      for (j=0; j<N; j++) {   
           printf("%3.2lf ", a[i*N+j]); 
      }
      printf("\n"); 
   }

   printf("El tiempo de ejeccion es %f sg\n", t/1000.0); 
   cudaFreeHost(a); cudaFreeHost(b); cudaFreeHost(c);    
}


