
#define NT 512 
#define NB 20


___global__ void kernelProductoPunto(double *a, double *b,
                                     double *r, int n) {
    int i = threadIdx.x+blockIdx.x*blockDim.x;
    double tmp=0.0;
    int x = threadIdx.x;
    __shared__ double cache[NT];
    int k;
    
    while (i < n) {
        tmp += a[i]*b[i];
        i += blockDim.x*gridDim.x;
    }
    cache[x] = tmp;
    __syncthreads();

    k = NT/2;
    
    while (k > 0) {
        if (x < k) {
            cache[x] + = cache[x+k];
        }
        __syncthreads();
        k /= 2;
    }
    
    if (x==0) {
        r[blockIdx.x] = cache[0];
    }
    
}

double productoPuntoEnDevice(double *a, double *b, int n) {
    double *aD, *bD, *rD, *rH;
    int size = n*sizeof(double);
    int sizeRes = NB*sizeof(double);
    int i;
    double tmp=0.p;
    
    cudaMalloc(&aD, size);
    cudaMalloc(&bD, size);
    cudaMalloc(&rD, sizeRes);
    rH = (double *)malloc(sizeRes);
    
    cudaMemcpy(aD, a, size, cudaMemcpyDefault);
    cudaMemcpy(bD, b, size, cudaMemcpyDefault);
    
    kernelProductoPunto<<<NB, NT>>>(aD, bD, rD, n);
    
    cudaMemcpy(rH, rD, sizeRes, cudaMemcpyDefault);
    
    for (i=0; i<NB; i++)
       tmp += rH[i];
 
    cudaFree(aD); cudaFree(bD); cudaFree(rD);
    free(rH);
    return tmp;
}
