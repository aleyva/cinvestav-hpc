// install gnuplot-x11
// sudo apt-get install gnuplot-x11
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <math.h> 

#define NUM_POINTS 10000
#define MIN_POINT 0
#define MAX_POINT 100
#define NUM_COMMANDS 2
//const double PI 3.1415926535897932385


void print_plot(double x[], double y[], int num_points){
    char * commandsForGnuplot[] = {"set title \"Circulo - Tecnica Montecarlo\"", "plot 'data.temp'"};    
    /*Opens an interface that one can use to send commands as if they were typing into the
     *     gnuplot command line.  "The -persistent" keeps the plot open even after your
     *     C program terminates.
     */
    FILE * temp = fopen("data.temp", "w");
    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
    int i;
    for (i=0; i < num_points; i++){
        fprintf(temp, "%lf %lf \n", x[i], y[i]); //Write the data to a temporary file
    }

    for (i=0; i < NUM_COMMANDS; i++){
        fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]); //Send commands to gnuplot one by one.
    }

    fflush(gnuplotPipe);
}


int main()
{   
    double inside_x[NUM_POINTS], inside_y[NUM_POINTS];
    double outside_x[NUM_POINTS], outside_y[NUM_POINTS];
    double rand_x, rand_y;
    double bound = MAX_POINT*MAX_POINT;
    srand(time(NULL));    
    int j = 0;
    
    for (int i = 0; i < NUM_POINTS; i++){
        rand_x = rand()%MAX_POINT; 
        rand_y = rand()%MAX_POINT;        

        if( pow(rand_x,2.0) + pow(rand_y,2.0) <= bound)
            inside_x[j] = rand_x;
            inside_y[j] = rand_y;
            j++;

    }

    print_plot(inside_x, inside_y, j);
    
    
    return 0;
}

