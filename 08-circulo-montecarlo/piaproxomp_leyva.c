#include <stdio.h>
#include <time.h>
#include <omp.h>
#include <stdlib.h>
#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)


float ran3(long *idum){
	
	static int inext,inextp;
	static long ma[56];
	static int iff=0;
	long mj,mk;
	int i,ii,k;
	if (*idum < 0 || iff == 0) {
		iff=1;
		mj=labs(MSEED-labs(*idum));
		mj %= MBIG;
		ma[55]=mj;
		mk=1;
		for (i=1;i<=54;i++) {
			ii=(21*i) % 55;
			ma[ii]=mk;
			mk=mj-mk;
			if (mk < MZ) mk += MBIG;
			mj=ma[ii];
		}
		for (k=1;k<=4;k++)
			for (i=1;i<=55;i++) {
				ma[i] -= ma[1+(i+30) % 55];
				if (ma[i] < MZ) ma[i] += MBIG;
			}
		inext=0;
		inextp=31;
		*idum=1;
	}
	if (++inext == 56) inext=1;
	if (++inextp == 56) inextp=1;
		mj=ma[inext]-ma[inextp];
	if (mj < MZ) mj += MBIG;
		ma[inext]=mj;
	*idum = mj;
	return (*idum)*FAC;
}


int main(int argc, char *argv[]){
	if(argc!=2){
		printf("<events>\n");
		exit(-1);
	}		
	int events = atoi(argv[1]);		
	long *seed1, *seed2;
	seed1 = (long*)malloc(sizeof(long) * 1);
	seed2 = (long*)malloc(sizeof(long) * 1);	
	*seed1 = 12324;
	*seed2 = 43213;

	printf("Starting omp: \n");
	int in=0;

	
	#pragma omp parallel for reduction(+:in)
		for(int i=0;i<events;i++){
			double x = ran3(seed1);	
			double y = ran3(seed2);				
			//printf("x: %10f\ty: %10f \n", x,y);			
			if(x*x + y*y <= 1)
				in++;
		}
	
	
	printf("Numero de puntos dentro: %d de %d \n", in, events);
	printf("Pi= %f\n", 4*((double)in)/((double)events));
	return 0;
}
